
# KeyPair

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**privateKey** | **String** | privateKey |  [optional]
**publicKey** | **String** | publicKey |  [optional]
**group** | [**Group**](Group.md) | group |  [optional]



