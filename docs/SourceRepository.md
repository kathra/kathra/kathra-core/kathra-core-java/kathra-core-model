
# SourceRepository

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**provider** | **String** | provider |  [optional]
**providerId** | **String** | providerId |  [optional]
**path** | **String** | path |  [optional]
**sshUrl** | **String** | sshUrl |  [optional]
**httpUrl** | **String** | httpUrl |  [optional]
**webUrl** | **String** | webUrl |  [optional]
**branchs** | **List&lt;String&gt;** | branchs |  [optional]



