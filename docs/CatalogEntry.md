
# CatalogEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** | Description |  [optional]
**icon** | **String** | Icon&#39;s URL |  [optional]
**packageTemplate** | [**PackageTemplateEnum**](#PackageTemplateEnum) | packageTemplate |  [optional]
**packages** | [**List&lt;CatalogEntryPackage&gt;**](CatalogEntryPackage.md) | packages |  [optional]


<a name="PackageTemplateEnum"></a>
## Enum: PackageTemplateEnum
Name | Value
---- | -----
SERVICE | &quot;REST_SERVICE&quot;



