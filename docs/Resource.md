
# Resource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | id |  [optional]
**name** | **String** | name |  [optional]
**status** | [**StatusEnum**](#StatusEnum) | status |  [optional]
**createdBy** | **String** | createdBy |  [optional]
**updatedBy** | **String** | updatedBy |  [optional]
**createdAt** | **Integer** | createdAt |  [optional]
**updatedAt** | **Integer** | updatedAt |  [optional]
**metadata** | **Map&lt;String, Object&gt;** | metadata |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
PENDING | &quot;PENDING&quot;
READY | &quot;READY&quot;
UPDATING | &quot;UPDATING&quot;
UNSTABLE | &quot;UNSTABLE&quot;
ERROR | &quot;ERROR&quot;
DELETED | &quot;DELETED&quot;



