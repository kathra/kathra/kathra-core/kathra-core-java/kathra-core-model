
# CatalogEntryPackage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**catalogEntry** | [**CatalogEntry**](CatalogEntry.md) | catalogEntry |  [optional]
**packageType** | [**PackageTypeEnum**](#PackageTypeEnum) | packageType |  [optional]
**provider** | **String** | Provider |  [optional]
**providerId** | **String** | ProviderId |  [optional]
**url** | **String** | String |  [optional]
**versions** | [**List&lt;CatalogEntryPackageVersion&gt;**](CatalogEntryPackageVersion.md) | versions |  [optional]


<a name="PackageTypeEnum"></a>
## Enum: PackageTypeEnum
Name | Value
---- | -----
HELM | &quot;HELM&quot;



