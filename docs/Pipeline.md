
# Pipeline

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**provider** | **String** | provider |  [optional]
**providerId** | **String** | providerId |  [optional]
**credentialId** | **String** | credentialId |  [optional]
**path** | **String** | path |  [optional]
**sourceRepository** | [**SourceRepository**](SourceRepository.md) | sourceRepository |  [optional]
**template** | [**TemplateEnum**](#TemplateEnum) | template |  [optional]


<a name="TemplateEnum"></a>
## Enum: TemplateEnum
Name | Value
---- | -----
JAVA_LIBRARY | &quot;JAVA_LIBRARY&quot;
PYTHON_LIBRARY | &quot;PYTHON_LIBRARY&quot;
JAVA_SERVICE | &quot;JAVA_SERVICE&quot;
PYTHON_SERVICE | &quot;PYTHON_SERVICE&quot;
HELM_PACKAGE | &quot;HELM_PACKAGE&quot;
DOCKER_SERVICE | &quot;DOCKER_SERVICE&quot;



