
# Asset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**binaryRepository** | [**BinaryRepository**](BinaryRepository.md) | binaryRepository |  [optional]
**sourceRepository** | [**SourceRepository**](SourceRepository.md) | sourceRepository |  [optional]
**pipeline** | [**Pipeline**](Pipeline.md) | pipeline |  [optional]



