
# Membership

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** | path |  [optional]
**memberType** | [**MemberTypeEnum**](#MemberTypeEnum) | memberType |  [optional]
**memberName** | **String** | memberName |  [optional]
**role** | [**RoleEnum**](#RoleEnum) | role |  [optional]


<a name="MemberTypeEnum"></a>
## Enum: MemberTypeEnum
Name | Value
---- | -----
USER | &quot;user&quot;
GROUP | &quot;group&quot;


<a name="RoleEnum"></a>
## Enum: RoleEnum
Name | Value
---- | -----
GUEST | &quot;guest&quot;
CONTRIBUTOR | &quot;contributor&quot;
MANAGER | &quot;manager&quot;



