
# Implementation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**component** | [**Component**](Component.md) | component |  [optional]
**description** | **String** | description |  [optional]
**language** | [**LanguageEnum**](#LanguageEnum) | language |  [optional]
**title** | **String** | title |  [optional]
**versions** | [**List&lt;ImplementationVersion&gt;**](ImplementationVersion.md) | versions |  [optional]
**catalogEntries** | [**List&lt;CatalogEntry&gt;**](CatalogEntry.md) | catalogEntries |  [optional]


<a name="LanguageEnum"></a>
## Enum: LanguageEnum
Name | Value
---- | -----
JAVA | &quot;JAVA&quot;
PYTHON | &quot;PYTHON&quot;



