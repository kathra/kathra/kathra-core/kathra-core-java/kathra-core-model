
# SourceRepositoryCommit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | id |  [optional]
**shortId** | **String** | shortId |  [optional]
**title** | **String** | title |  [optional]
**authorName** | **String** | authorName |  [optional]
**authorEmail** | **String** | authorEmail |  [optional]
**createdAt** | **String** | createdAt |  [optional]
**committerName** | **String** | committerName |  [optional]
**committerEmail** | **String** | committerEmail |  [optional]
**message** | **String** | message |  [optional]



