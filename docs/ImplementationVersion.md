
# ImplementationVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceRepo** | [**SourceRepository**](SourceRepository.md) | sourceRepo |  [optional]
**version** | **String** | version |  [optional]
**implementation** | [**Implementation**](Implementation.md) | implementation |  [optional]
**apiVersion** | [**ApiVersion**](ApiVersion.md) | apiVersion |  [optional]



