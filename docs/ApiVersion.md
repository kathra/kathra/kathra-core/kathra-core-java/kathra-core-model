
# ApiVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**component** | [**Component**](Component.md) | component |  [optional]
**released** | **Boolean** | released |  [optional]
**version** | **String** | version |  [optional]
**apiRepositoryStatus** | [**ApiRepositoryStatusEnum**](#ApiRepositoryStatusEnum) | apiRepositoryStatus |  [optional]
**librariesApiVersions** | [**List&lt;LibraryApiVersion&gt;**](LibraryApiVersion.md) | librariesApiVersions |  [optional]
**implementationsVersions** | [**List&lt;ImplementationVersion&gt;**](ImplementationVersion.md) | implementationsVersions |  [optional]


<a name="ApiRepositoryStatusEnum"></a>
## Enum: ApiRepositoryStatusEnum
Name | Value
---- | -----
PENDING | &quot;PENDING&quot;
READY | &quot;READY&quot;
UPDATING | &quot;UPDATING&quot;
UNSTABLE | &quot;UNSTABLE&quot;
ERROR | &quot;ERROR&quot;
DELETED | &quot;DELETED&quot;



