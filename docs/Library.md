
# Library

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**component** | [**Component**](Component.md) | component |  [optional]
**type** | [**TypeEnum**](#TypeEnum) | type |  [optional]
**language** | [**LanguageEnum**](#LanguageEnum) | language |  [optional]
**versions** | [**List&lt;LibraryApiVersion&gt;**](LibraryApiVersion.md) | versions |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
MODEL | &quot;MODEL&quot;
INTERFACE | &quot;INTERFACE&quot;
CLIENT | &quot;CLIENT&quot;


<a name="LanguageEnum"></a>
## Enum: LanguageEnum
Name | Value
---- | -----
JAVA | &quot;JAVA&quot;
PYTHON | &quot;PYTHON&quot;



