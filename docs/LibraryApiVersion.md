
# LibraryApiVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**library** | [**Library**](Library.md) | library |  [optional]
**apiVersion** | [**ApiVersion**](ApiVersion.md) | apiVersion |  [optional]
**apiRepositoryStatus** | [**ApiRepositoryStatusEnum**](#ApiRepositoryStatusEnum) | apiRepositoryStatus |  [optional]
**pipelineStatus** | [**PipelineStatusEnum**](#PipelineStatusEnum) | pipelineStatus |  [optional]


<a name="ApiRepositoryStatusEnum"></a>
## Enum: ApiRepositoryStatusEnum
Name | Value
---- | -----
PENDING | &quot;PENDING&quot;
READY | &quot;READY&quot;
UPDATING | &quot;UPDATING&quot;
UNSTABLE | &quot;UNSTABLE&quot;
ERROR | &quot;ERROR&quot;
DELETED | &quot;DELETED&quot;


<a name="PipelineStatusEnum"></a>
## Enum: PipelineStatusEnum
Name | Value
---- | -----
PENDING | &quot;PENDING&quot;
READY | &quot;READY&quot;
UPDATING | &quot;UPDATING&quot;
UNSTABLE | &quot;UNSTABLE&quot;
ERROR | &quot;ERROR&quot;
DELETED | &quot;DELETED&quot;



