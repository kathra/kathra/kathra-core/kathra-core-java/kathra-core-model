
# CatalogEntryArgument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** | Readable argument for catalog entry |  [optional]
**description** | **String** | Description argument for catalog entry |  [optional]
**key** | **String** | Argument key for catalog entry |  [optional]
**value** | **String** | Argument value for catalog entry |  [optional]
**contrainst** | **String** | Argument constraint for catalog entry |  [optional]



