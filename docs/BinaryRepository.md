
# BinaryRepository

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | [**Group**](Group.md) | group |  [optional]
**type** | [**TypeEnum**](#TypeEnum) | type |  [optional]
**provider** | **String** | provider |  [optional]
**providerId** | **String** | providerId |  [optional]
**url** | **String** | url |  [optional]
**snapshot** | **Boolean** | snapshot |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
JAVA | &quot;JAVA&quot;
PYTHON | &quot;PYTHON&quot;
DOCKER_IMAGE | &quot;DOCKER_IMAGE&quot;
HELM | &quot;HELM&quot;



