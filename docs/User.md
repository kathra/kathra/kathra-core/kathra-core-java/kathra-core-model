
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** | firstName |  [optional]
**lastName** | **String** | lastName |  [optional]
**email** | **String** | email |  [optional]
**password** | **String** | password |  [optional]
**phone** | **String** | phone |  [optional]
**groups** | [**List&lt;Assignation&gt;**](Assignation.md) | groups |  [optional]



