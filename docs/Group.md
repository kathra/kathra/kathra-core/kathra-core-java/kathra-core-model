
# Group

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** | Group&#39;s path in case of subgroup |  [optional]
**technicalUser** | [**User**](User.md) | technicalUser |  [optional]
**binaryRepositories** | [**List&lt;BinaryRepository&gt;**](BinaryRepository.md) | binaryRepositories |  [optional]
**members** | [**List&lt;Assignation&gt;**](Assignation.md) | members |  [optional]
**parent** | [**Group**](Group.md) | parent |  [optional]
**sourceRepositoryStatus** | [**SourceRepositoryStatusEnum**](#SourceRepositoryStatusEnum) | sourceRepositoryStatus |  [optional]
**sourceMembershipStatus** | [**SourceMembershipStatusEnum**](#SourceMembershipStatusEnum) | sourceMembershipStatus |  [optional]
**pipelineFolderStatus** | [**PipelineFolderStatusEnum**](#PipelineFolderStatusEnum) | pipelineFolderStatus |  [optional]
**binaryRepositoryStatus** | [**BinaryRepositoryStatusEnum**](#BinaryRepositoryStatusEnum) | binaryRepositoryStatus |  [optional]


<a name="SourceRepositoryStatusEnum"></a>
## Enum: SourceRepositoryStatusEnum
Name | Value
---- | -----
PENDING | &quot;PENDING&quot;
READY | &quot;READY&quot;
UPDATING | &quot;UPDATING&quot;
UNSTABLE | &quot;UNSTABLE&quot;
ERROR | &quot;ERROR&quot;
DELETED | &quot;DELETED&quot;


<a name="SourceMembershipStatusEnum"></a>
## Enum: SourceMembershipStatusEnum
Name | Value
---- | -----
PENDING | &quot;PENDING&quot;
READY | &quot;READY&quot;
UPDATING | &quot;UPDATING&quot;
UNSTABLE | &quot;UNSTABLE&quot;
ERROR | &quot;ERROR&quot;
DELETED | &quot;DELETED&quot;


<a name="PipelineFolderStatusEnum"></a>
## Enum: PipelineFolderStatusEnum
Name | Value
---- | -----
PENDING | &quot;PENDING&quot;
READY | &quot;READY&quot;
UPDATING | &quot;UPDATING&quot;
UNSTABLE | &quot;UNSTABLE&quot;
ERROR | &quot;ERROR&quot;
DELETED | &quot;DELETED&quot;


<a name="BinaryRepositoryStatusEnum"></a>
## Enum: BinaryRepositoryStatusEnum
Name | Value
---- | -----
PENDING | &quot;PENDING&quot;
READY | &quot;READY&quot;
UPDATING | &quot;UPDATING&quot;
UNSTABLE | &quot;UNSTABLE&quot;
ERROR | &quot;ERROR&quot;
DELETED | &quot;DELETED&quot;



