
# PackageTemplateArgument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | Argument key to generate catalog entry |  [optional]
**value** | **String** | Argument value to generate catalog entry |  [optional]
**contrainst** | **String** | Argument constraint |  [optional]



