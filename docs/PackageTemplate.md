
# PackageTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Name |  [optional]
**arguments** | [**List&lt;PackageTemplateArgument&gt;**](PackageTemplateArgument.md) | Catalog entry arguments |  [optional]



