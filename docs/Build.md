
# Build

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buildNumber** | **String** | Build identifier |  [optional]
**path** | **String** | Pipeline path |  [optional]
**commitLabel** | **String** | Branch or Tag |  [optional]
**commitId** | **String** | Commit id |  [optional]
**creationDate** | **Long** | Build date (UTC timestamp) |  [optional]
**duration** | **Integer** | Duration in seconds |  [optional]
**status** | [**StatusEnum**](#StatusEnum) | Build status |  [optional]
**buildArguments** | [**List&lt;BuildArgument&gt;**](BuildArgument.md) | Additional Build arguments |  [optional]
**logs** | **String** | Build logs |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
SCHEDULED | &quot;scheduled&quot;
PROCESSING | &quot;processing&quot;
SUCCESS | &quot;success&quot;
FAILED | &quot;failed&quot;



