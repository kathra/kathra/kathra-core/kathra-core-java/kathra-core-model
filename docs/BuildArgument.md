
# BuildArgument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | Build argument key |  [optional]
**value** | **String** | Build argument value |  [optional]



