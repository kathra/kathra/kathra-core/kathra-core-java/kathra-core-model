
# CatalogEntryPackageVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**catalogEntryPackage** | [**CatalogEntryPackage**](CatalogEntryPackage.md) | catalogEntryPackage |  [optional]
**version** | **String** | Version |  [optional]
**documentation** | **String** | Documentation of catalog entry (Markdown) |  [optional]
**arguments** | [**List&lt;CatalogEntryArgument&gt;**](CatalogEntryArgument.md) | Catalog entry arguments |  [optional]



