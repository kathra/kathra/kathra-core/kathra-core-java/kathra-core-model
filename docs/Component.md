
# Component

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** | description |  [optional]
**title** | **String** | title |  [optional]
**versions** | [**List&lt;ApiVersion&gt;**](ApiVersion.md) | versions |  [optional]
**libraries** | [**List&lt;Library&gt;**](Library.md) | libraries |  [optional]
**implementations** | [**List&lt;Implementation&gt;**](Implementation.md) | implementations |  [optional]
**apiRepository** | [**SourceRepository**](SourceRepository.md) | apiRepository |  [optional]



