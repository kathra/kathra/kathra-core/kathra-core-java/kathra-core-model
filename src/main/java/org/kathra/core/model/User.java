/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* User
*/
@ApiModel(description = "User")
public class User extends Resource {

    protected String firstName = null;
    protected String lastName = null;
    protected String email = null;
    protected String password = null;
    protected String phone = null;
    protected List<Assignation> groups = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myUser.id(String).anotherQuickSetter(..))
    * @param String id
    * @return User The modified User object
    **/
    public User id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myUser.name(String).anotherQuickSetter(..))
    * @param String name
    * @return User The modified User object
    **/
    public User name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myUser.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return User The modified User object
    **/
    public User status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myUser.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return User The modified User object
    **/
    public User createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myUser.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return User The modified User object
    **/
    public User updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myUser.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return User The modified User object
    **/
    public User createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myUser.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return User The modified User object
    **/
    public User updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myUser.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return User The modified User object
    **/
    public User metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public User putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set firstName
    * Useful setter to use in builder style (eg. myUser.firstName(String).anotherQuickSetter(..))
    * @param String firstName
    * @return User The modified User object
    **/
    public User firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
    * firstName
    * @return String firstName
    **/
    @ApiModelProperty(value = "firstName")
    public String getFirstName() {
        return firstName;
    }

    /**
    * firstName
    **/
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
    * Quick Set lastName
    * Useful setter to use in builder style (eg. myUser.lastName(String).anotherQuickSetter(..))
    * @param String lastName
    * @return User The modified User object
    **/
    public User lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
    * lastName
    * @return String lastName
    **/
    @ApiModelProperty(value = "lastName")
    public String getLastName() {
        return lastName;
    }

    /**
    * lastName
    **/
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
    * Quick Set email
    * Useful setter to use in builder style (eg. myUser.email(String).anotherQuickSetter(..))
    * @param String email
    * @return User The modified User object
    **/
    public User email(String email) {
        this.email = email;
        return this;
    }

    /**
    * email
    * @return String email
    **/
    @ApiModelProperty(value = "email")
    public String getEmail() {
        return email;
    }

    /**
    * email
    **/
    public void setEmail(String email) {
        this.email = email;
    }

    /**
    * Quick Set password
    * Useful setter to use in builder style (eg. myUser.password(String).anotherQuickSetter(..))
    * @param String password
    * @return User The modified User object
    **/
    public User password(String password) {
        this.password = password;
        return this;
    }

    /**
    * password
    * @return String password
    **/
    @ApiModelProperty(value = "password")
    public String getPassword() {
        return password;
    }

    /**
    * password
    **/
    public void setPassword(String password) {
        this.password = password;
    }

    /**
    * Quick Set phone
    * Useful setter to use in builder style (eg. myUser.phone(String).anotherQuickSetter(..))
    * @param String phone
    * @return User The modified User object
    **/
    public User phone(String phone) {
        this.phone = phone;
        return this;
    }

    /**
    * phone
    * @return String phone
    **/
    @ApiModelProperty(value = "phone")
    public String getPhone() {
        return phone;
    }

    /**
    * phone
    **/
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
    * Quick Set groups
    * Useful setter to use in builder style (eg. myUser.groups(List<Assignation>).anotherQuickSetter(..))
    * @param List<Assignation> groups
    * @return User The modified User object
    **/
    public User groups(List<Assignation> groups) {
        this.groups = groups;
        return this;
    }

    public User addGroupsItem(Assignation groupsItem) {
        if (this.groups == null) {
            this.groups = new ArrayList();
        }
        this.groups.add(groupsItem);
        return this;
    }

    /**
    * groups
    * @return List<Assignation> groups
    **/
    @ApiModelProperty(value = "groups")
    public List<Assignation> getGroups() {
        return groups;
    }

    /**
    * groups
    **/
    public void setGroups(List<Assignation> groups) {
        this.groups = groups;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(this.firstName, user.firstName) &&
        Objects.equals(this.lastName, user.lastName) &&
        Objects.equals(this.email, user.email) &&
        Objects.equals(this.password, user.password) &&
        Objects.equals(this.phone, user.phone) &&
        Objects.equals(this.groups, user.groups) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, email, password, phone, groups, super.hashCode());
    }
}

