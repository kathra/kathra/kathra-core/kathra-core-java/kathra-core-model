/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import java.util.Objects;

/**
* ImplementationVersion
*/
@ApiModel(description = "ImplementationVersion")
public class ImplementationVersion extends Resource {

    protected SourceRepository sourceRepo = null;
    protected String version = null;
    protected Implementation implementation = null;
    protected ApiVersion apiVersion = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myImplementationVersion.id(String).anotherQuickSetter(..))
    * @param String id
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myImplementationVersion.name(String).anotherQuickSetter(..))
    * @param String name
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myImplementationVersion.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myImplementationVersion.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myImplementationVersion.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myImplementationVersion.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myImplementationVersion.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myImplementationVersion.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public ImplementationVersion putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set sourceRepo
    * Useful setter to use in builder style (eg. myImplementationVersion.sourceRepo(SourceRepository).anotherQuickSetter(..))
    * @param SourceRepository sourceRepo
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion sourceRepo(SourceRepository sourceRepo) {
        this.sourceRepo = sourceRepo;
        return this;
    }

    /**
    * sourceRepo
    * @return SourceRepository sourceRepo
    **/
    @ApiModelProperty(value = "sourceRepo")
    public SourceRepository getSourceRepo() {
        return sourceRepo;
    }

    /**
    * sourceRepo
    **/
    public void setSourceRepo(SourceRepository sourceRepo) {
        this.sourceRepo = sourceRepo;
    }

    /**
    * Quick Set version
    * Useful setter to use in builder style (eg. myImplementationVersion.version(String).anotherQuickSetter(..))
    * @param String version
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion version(String version) {
        this.version = version;
        return this;
    }

    /**
    * version
    * @return String version
    **/
    @ApiModelProperty(value = "version")
    public String getVersion() {
        return version;
    }

    /**
    * version
    **/
    public void setVersion(String version) {
        this.version = version;
    }

    /**
    * Quick Set implementation
    * Useful setter to use in builder style (eg. myImplementationVersion.implementation(Implementation).anotherQuickSetter(..))
    * @param Implementation implementation
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion implementation(Implementation implementation) {
        this.implementation = implementation;
        return this;
    }

    /**
    * implementation
    * @return Implementation implementation
    **/
    @ApiModelProperty(value = "implementation")
    public Implementation getImplementation() {
        return implementation;
    }

    /**
    * implementation
    **/
    public void setImplementation(Implementation implementation) {
        this.implementation = implementation;
    }

    /**
    * Quick Set apiVersion
    * Useful setter to use in builder style (eg. myImplementationVersion.apiVersion(ApiVersion).anotherQuickSetter(..))
    * @param ApiVersion apiVersion
    * @return ImplementationVersion The modified ImplementationVersion object
    **/
    public ImplementationVersion apiVersion(ApiVersion apiVersion) {
        this.apiVersion = apiVersion;
        return this;
    }

    /**
    * apiVersion
    * @return ApiVersion apiVersion
    **/
    @ApiModelProperty(value = "apiVersion")
    public ApiVersion getApiVersion() {
        return apiVersion;
    }

    /**
    * apiVersion
    **/
    public void setApiVersion(ApiVersion apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ImplementationVersion implementationVersion = (ImplementationVersion) o;
        return Objects.equals(this.sourceRepo, implementationVersion.sourceRepo) &&
        Objects.equals(this.version, implementationVersion.version) &&
        Objects.equals(this.implementation, implementationVersion.implementation) &&
        Objects.equals(this.apiVersion, implementationVersion.apiVersion) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceRepo, version, implementation, apiVersion, super.hashCode());
    }
}

