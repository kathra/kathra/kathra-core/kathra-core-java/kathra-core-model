/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* SourceRepositoryCommit
*/
@ApiModel(description = "SourceRepositoryCommit")
public class SourceRepositoryCommit {

    protected String id = null;
    protected String shortId = null;
    protected String title = null;
    protected String authorName = null;
    protected String authorEmail = null;
    protected String createdAt = null;
    protected String committerName = null;
    protected String committerEmail = null;
    protected String message = null;


    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. mySourceRepositoryCommit.id(String).anotherQuickSetter(..))
    * @param String id
    * @return SourceRepositoryCommit The modified SourceRepositoryCommit object
    **/
    public SourceRepositoryCommit id(String id) {
        this.id = id;
        return this;
    }

    /**
    * id
    * @return String id
    **/
    @ApiModelProperty(value = "id")
    public String getId() {
        return id;
    }

    /**
    * id
    **/
    public void setId(String id) {
        this.id = id;
    }

    /**
    * Quick Set shortId
    * Useful setter to use in builder style (eg. mySourceRepositoryCommit.shortId(String).anotherQuickSetter(..))
    * @param String shortId
    * @return SourceRepositoryCommit The modified SourceRepositoryCommit object
    **/
    public SourceRepositoryCommit shortId(String shortId) {
        this.shortId = shortId;
        return this;
    }

    /**
    * shortId
    * @return String shortId
    **/
    @ApiModelProperty(value = "shortId")
    public String getShortId() {
        return shortId;
    }

    /**
    * shortId
    **/
    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    /**
    * Quick Set title
    * Useful setter to use in builder style (eg. mySourceRepositoryCommit.title(String).anotherQuickSetter(..))
    * @param String title
    * @return SourceRepositoryCommit The modified SourceRepositoryCommit object
    **/
    public SourceRepositoryCommit title(String title) {
        this.title = title;
        return this;
    }

    /**
    * title
    * @return String title
    **/
    @ApiModelProperty(value = "title")
    public String getTitle() {
        return title;
    }

    /**
    * title
    **/
    public void setTitle(String title) {
        this.title = title;
    }

    /**
    * Quick Set authorName
    * Useful setter to use in builder style (eg. mySourceRepositoryCommit.authorName(String).anotherQuickSetter(..))
    * @param String authorName
    * @return SourceRepositoryCommit The modified SourceRepositoryCommit object
    **/
    public SourceRepositoryCommit authorName(String authorName) {
        this.authorName = authorName;
        return this;
    }

    /**
    * authorName
    * @return String authorName
    **/
    @ApiModelProperty(value = "authorName")
    public String getAuthorName() {
        return authorName;
    }

    /**
    * authorName
    **/
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    /**
    * Quick Set authorEmail
    * Useful setter to use in builder style (eg. mySourceRepositoryCommit.authorEmail(String).anotherQuickSetter(..))
    * @param String authorEmail
    * @return SourceRepositoryCommit The modified SourceRepositoryCommit object
    **/
    public SourceRepositoryCommit authorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
        return this;
    }

    /**
    * authorEmail
    * @return String authorEmail
    **/
    @ApiModelProperty(value = "authorEmail")
    public String getAuthorEmail() {
        return authorEmail;
    }

    /**
    * authorEmail
    **/
    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. mySourceRepositoryCommit.createdAt(String).anotherQuickSetter(..))
    * @param String createdAt
    * @return SourceRepositoryCommit The modified SourceRepositoryCommit object
    **/
    public SourceRepositoryCommit createdAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * createdAt
    * @return String createdAt
    **/
    @ApiModelProperty(value = "createdAt")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
    * createdAt
    **/
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
    * Quick Set committerName
    * Useful setter to use in builder style (eg. mySourceRepositoryCommit.committerName(String).anotherQuickSetter(..))
    * @param String committerName
    * @return SourceRepositoryCommit The modified SourceRepositoryCommit object
    **/
    public SourceRepositoryCommit committerName(String committerName) {
        this.committerName = committerName;
        return this;
    }

    /**
    * committerName
    * @return String committerName
    **/
    @ApiModelProperty(value = "committerName")
    public String getCommitterName() {
        return committerName;
    }

    /**
    * committerName
    **/
    public void setCommitterName(String committerName) {
        this.committerName = committerName;
    }

    /**
    * Quick Set committerEmail
    * Useful setter to use in builder style (eg. mySourceRepositoryCommit.committerEmail(String).anotherQuickSetter(..))
    * @param String committerEmail
    * @return SourceRepositoryCommit The modified SourceRepositoryCommit object
    **/
    public SourceRepositoryCommit committerEmail(String committerEmail) {
        this.committerEmail = committerEmail;
        return this;
    }

    /**
    * committerEmail
    * @return String committerEmail
    **/
    @ApiModelProperty(value = "committerEmail")
    public String getCommitterEmail() {
        return committerEmail;
    }

    /**
    * committerEmail
    **/
    public void setCommitterEmail(String committerEmail) {
        this.committerEmail = committerEmail;
    }

    /**
    * Quick Set message
    * Useful setter to use in builder style (eg. mySourceRepositoryCommit.message(String).anotherQuickSetter(..))
    * @param String message
    * @return SourceRepositoryCommit The modified SourceRepositoryCommit object
    **/
    public SourceRepositoryCommit message(String message) {
        this.message = message;
        return this;
    }

    /**
    * message
    * @return String message
    **/
    @ApiModelProperty(value = "message")
    public String getMessage() {
        return message;
    }

    /**
    * message
    **/
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SourceRepositoryCommit sourceRepositoryCommit = (SourceRepositoryCommit) o;
        return Objects.equals(this.id, sourceRepositoryCommit.id) &&
        Objects.equals(this.shortId, sourceRepositoryCommit.shortId) &&
        Objects.equals(this.title, sourceRepositoryCommit.title) &&
        Objects.equals(this.authorName, sourceRepositoryCommit.authorName) &&
        Objects.equals(this.authorEmail, sourceRepositoryCommit.authorEmail) &&
        Objects.equals(this.createdAt, sourceRepositoryCommit.createdAt) &&
        Objects.equals(this.committerName, sourceRepositoryCommit.committerName) &&
        Objects.equals(this.committerEmail, sourceRepositoryCommit.committerEmail) &&
        Objects.equals(this.message, sourceRepositoryCommit.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, shortId, title, authorName, authorEmail, createdAt, committerName, committerEmail, message);
    }
}

