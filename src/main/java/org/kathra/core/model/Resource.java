/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
* Resource
*/
@ApiModel(description = "Resource")
public class Resource {

    protected String id = null;
    protected String name = null;
    /**
    * status
    */
    public enum StatusEnum {
        PENDING("PENDING"),
        
        READY("READY"),
        
        UPDATING("UPDATING"),
        
        UNSTABLE("UNSTABLE"),
        
        ERROR("ERROR"),
        
        DELETED("DELETED");

        private String value;

        StatusEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static StatusEnum fromValue(String text) {
            for (StatusEnum b : StatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected StatusEnum status = null;
    protected String createdBy = null;
    protected String updatedBy = null;
    protected Integer createdAt = null;
    protected Integer updatedAt = null;
    protected Map<String, Object> metadata = null;


    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myResource.id(String).anotherQuickSetter(..))
    * @param String id
    * @return Resource The modified Resource object
    **/
    public Resource id(String id) {
        this.id = id;
        return this;
    }

    /**
    * id
    * @return String id
    **/
    @ApiModelProperty(value = "id")
    public String getId() {
        return id;
    }

    /**
    * id
    **/
    public void setId(String id) {
        this.id = id;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myResource.name(String).anotherQuickSetter(..))
    * @param String name
    * @return Resource The modified Resource object
    **/
    public Resource name(String name) {
        this.name = name;
        return this;
    }

    /**
    * name
    * @return String name
    **/
    @ApiModelProperty(value = "name")
    public String getName() {
        return name;
    }

    /**
    * name
    **/
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myResource.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return Resource The modified Resource object
    **/
    public Resource status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * status
    * @return StatusEnum status
    **/
    @ApiModelProperty(value = "status")
    public StatusEnum getStatus() {
        return status;
    }

    /**
    * status
    **/
    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myResource.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return Resource The modified Resource object
    **/
    public Resource createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * createdBy
    * @return String createdBy
    **/
    @ApiModelProperty(value = "createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    /**
    * createdBy
    **/
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myResource.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return Resource The modified Resource object
    **/
    public Resource updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * updatedBy
    * @return String updatedBy
    **/
    @ApiModelProperty(value = "updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
    * updatedBy
    **/
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myResource.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return Resource The modified Resource object
    **/
    public Resource createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * createdAt
    * @return Integer createdAt
    **/
    @ApiModelProperty(value = "createdAt")
    public Integer getCreatedAt() {
        return createdAt;
    }

    /**
    * createdAt
    **/
    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myResource.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return Resource The modified Resource object
    **/
    public Resource updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * updatedAt
    * @return Integer updatedAt
    **/
    @ApiModelProperty(value = "updatedAt")
    public Integer getUpdatedAt() {
        return updatedAt;
    }

    /**
    * updatedAt
    **/
    public void setUpdatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myResource.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return Resource The modified Resource object
    **/
    public Resource metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public Resource putMetadataItem(String key, Object metadataItem) {
        if (this.metadata == null) {
            this.metadata = new HashMap();
        }
        this.metadata.put(key, metadataItem);
        return this;
    }

    /**
    * metadata
    * @return Map<String, Object> metadata
    **/
    @ApiModelProperty(value = "metadata")
    public Map<String, Object> getMetadata() {
        return metadata;
    }

    /**
    * metadata
    **/
    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Resource resource = (Resource) o;
        return Objects.equals(this.id, resource.id) &&
        Objects.equals(this.name, resource.name) &&
        Objects.equals(this.status, resource.status) &&
        Objects.equals(this.createdBy, resource.createdBy) &&
        Objects.equals(this.updatedBy, resource.updatedBy) &&
        Objects.equals(this.createdAt, resource.createdAt) &&
        Objects.equals(this.updatedAt, resource.updatedAt) &&
        Objects.equals(this.metadata, resource.metadata);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, status, createdBy, updatedBy, createdAt, updatedAt, metadata);
    }
}

