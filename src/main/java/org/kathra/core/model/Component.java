/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* Component
*/
@ApiModel(description = "Component")
public class Component extends Resource {

    protected String description = null;
    protected String title = null;
    protected List<ApiVersion> versions = null;
    protected List<Library> libraries = null;
    protected List<Implementation> implementations = null;
    protected SourceRepository apiRepository = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myComponent.id(String).anotherQuickSetter(..))
    * @param String id
    * @return Component The modified Component object
    **/
    public Component id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myComponent.name(String).anotherQuickSetter(..))
    * @param String name
    * @return Component The modified Component object
    **/
    public Component name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myComponent.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return Component The modified Component object
    **/
    public Component status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myComponent.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return Component The modified Component object
    **/
    public Component createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myComponent.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return Component The modified Component object
    **/
    public Component updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myComponent.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return Component The modified Component object
    **/
    public Component createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myComponent.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return Component The modified Component object
    **/
    public Component updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myComponent.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return Component The modified Component object
    **/
    public Component metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public Component putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set description
    * Useful setter to use in builder style (eg. myComponent.description(String).anotherQuickSetter(..))
    * @param String description
    * @return Component The modified Component object
    **/
    public Component description(String description) {
        this.description = description;
        return this;
    }

    /**
    * description
    * @return String description
    **/
    @ApiModelProperty(value = "description")
    public String getDescription() {
        return description;
    }

    /**
    * description
    **/
    public void setDescription(String description) {
        this.description = description;
    }

    /**
    * Quick Set title
    * Useful setter to use in builder style (eg. myComponent.title(String).anotherQuickSetter(..))
    * @param String title
    * @return Component The modified Component object
    **/
    public Component title(String title) {
        this.title = title;
        return this;
    }

    /**
    * title
    * @return String title
    **/
    @ApiModelProperty(value = "title")
    public String getTitle() {
        return title;
    }

    /**
    * title
    **/
    public void setTitle(String title) {
        this.title = title;
    }

    /**
    * Quick Set versions
    * Useful setter to use in builder style (eg. myComponent.versions(List<ApiVersion>).anotherQuickSetter(..))
    * @param List<ApiVersion> versions
    * @return Component The modified Component object
    **/
    public Component versions(List<ApiVersion> versions) {
        this.versions = versions;
        return this;
    }

    public Component addVersionsItem(ApiVersion versionsItem) {
        if (this.versions == null) {
            this.versions = new ArrayList();
        }
        this.versions.add(versionsItem);
        return this;
    }

    /**
    * versions
    * @return List<ApiVersion> versions
    **/
    @ApiModelProperty(value = "versions")
    public List<ApiVersion> getVersions() {
        return versions;
    }

    /**
    * versions
    **/
    public void setVersions(List<ApiVersion> versions) {
        this.versions = versions;
    }

    /**
    * Quick Set libraries
    * Useful setter to use in builder style (eg. myComponent.libraries(List<Library>).anotherQuickSetter(..))
    * @param List<Library> libraries
    * @return Component The modified Component object
    **/
    public Component libraries(List<Library> libraries) {
        this.libraries = libraries;
        return this;
    }

    public Component addLibrariesItem(Library librariesItem) {
        if (this.libraries == null) {
            this.libraries = new ArrayList();
        }
        this.libraries.add(librariesItem);
        return this;
    }

    /**
    * libraries
    * @return List<Library> libraries
    **/
    @ApiModelProperty(value = "libraries")
    public List<Library> getLibraries() {
        return libraries;
    }

    /**
    * libraries
    **/
    public void setLibraries(List<Library> libraries) {
        this.libraries = libraries;
    }

    /**
    * Quick Set implementations
    * Useful setter to use in builder style (eg. myComponent.implementations(List<Implementation>).anotherQuickSetter(..))
    * @param List<Implementation> implementations
    * @return Component The modified Component object
    **/
    public Component implementations(List<Implementation> implementations) {
        this.implementations = implementations;
        return this;
    }

    public Component addImplementationsItem(Implementation implementationsItem) {
        if (this.implementations == null) {
            this.implementations = new ArrayList();
        }
        this.implementations.add(implementationsItem);
        return this;
    }

    /**
    * implementations
    * @return List<Implementation> implementations
    **/
    @ApiModelProperty(value = "implementations")
    public List<Implementation> getImplementations() {
        return implementations;
    }

    /**
    * implementations
    **/
    public void setImplementations(List<Implementation> implementations) {
        this.implementations = implementations;
    }

    /**
    * Quick Set apiRepository
    * Useful setter to use in builder style (eg. myComponent.apiRepository(SourceRepository).anotherQuickSetter(..))
    * @param SourceRepository apiRepository
    * @return Component The modified Component object
    **/
    public Component apiRepository(SourceRepository apiRepository) {
        this.apiRepository = apiRepository;
        return this;
    }

    /**
    * apiRepository
    * @return SourceRepository apiRepository
    **/
    @ApiModelProperty(value = "apiRepository")
    public SourceRepository getApiRepository() {
        return apiRepository;
    }

    /**
    * apiRepository
    **/
    public void setApiRepository(SourceRepository apiRepository) {
        this.apiRepository = apiRepository;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Component component = (Component) o;
        return Objects.equals(this.description, component.description) &&
        Objects.equals(this.title, component.title) &&
        Objects.equals(this.versions, component.versions) &&
        Objects.equals(this.libraries, component.libraries) &&
        Objects.equals(this.implementations, component.implementations) &&
        Objects.equals(this.apiRepository, component.apiRepository) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, title, versions, libraries, implementations, apiRepository, super.hashCode());
    }
}

