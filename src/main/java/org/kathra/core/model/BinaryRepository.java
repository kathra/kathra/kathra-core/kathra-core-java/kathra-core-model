/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import java.util.Objects;

/**
* BinaryRepository
*/
@ApiModel(description = "BinaryRepository")
public class BinaryRepository extends Resource {

    protected Group group = null;
    /**
    * type
    */
    public enum TypeEnum {
        JAVA("JAVA"),
        
        PYTHON("PYTHON"),
        
        DOCKER_IMAGE("DOCKER_IMAGE"),
        
        HELM("HELM");

        private String value;

        TypeEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static TypeEnum fromValue(String text) {
            for (TypeEnum b : TypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected TypeEnum type = null;
    protected String provider = null;
    protected String providerId = null;
    protected String url = null;
    protected Boolean snapshot = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myBinaryRepository.id(String).anotherQuickSetter(..))
    * @param String id
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myBinaryRepository.name(String).anotherQuickSetter(..))
    * @param String name
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myBinaryRepository.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myBinaryRepository.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myBinaryRepository.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myBinaryRepository.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myBinaryRepository.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myBinaryRepository.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public BinaryRepository putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set group
    * Useful setter to use in builder style (eg. myBinaryRepository.group(Group).anotherQuickSetter(..))
    * @param Group group
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository group(Group group) {
        this.group = group;
        return this;
    }

    /**
    * group
    * @return Group group
    **/
    @ApiModelProperty(value = "group")
    public Group getGroup() {
        return group;
    }

    /**
    * group
    **/
    public void setGroup(Group group) {
        this.group = group;
    }

    /**
    * Quick Set type
    * Useful setter to use in builder style (eg. myBinaryRepository.type(TypeEnum).anotherQuickSetter(..))
    * @param TypeEnum type
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository type(TypeEnum type) {
        this.type = type;
        return this;
    }

    /**
    * type
    * @return TypeEnum type
    **/
    @ApiModelProperty(value = "type")
    public TypeEnum getType() {
        return type;
    }

    /**
    * type
    **/
    public void setType(TypeEnum type) {
        this.type = type;
    }

    /**
    * Quick Set provider
    * Useful setter to use in builder style (eg. myBinaryRepository.provider(String).anotherQuickSetter(..))
    * @param String provider
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository provider(String provider) {
        this.provider = provider;
        return this;
    }

    /**
    * provider
    * @return String provider
    **/
    @ApiModelProperty(value = "provider")
    public String getProvider() {
        return provider;
    }

    /**
    * provider
    **/
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
    * Quick Set providerId
    * Useful setter to use in builder style (eg. myBinaryRepository.providerId(String).anotherQuickSetter(..))
    * @param String providerId
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository providerId(String providerId) {
        this.providerId = providerId;
        return this;
    }

    /**
    * providerId
    * @return String providerId
    **/
    @ApiModelProperty(value = "providerId")
    public String getProviderId() {
        return providerId;
    }

    /**
    * providerId
    **/
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    /**
    * Quick Set url
    * Useful setter to use in builder style (eg. myBinaryRepository.url(String).anotherQuickSetter(..))
    * @param String url
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository url(String url) {
        this.url = url;
        return this;
    }

    /**
    * url
    * @return String url
    **/
    @ApiModelProperty(value = "url")
    public String getUrl() {
        return url;
    }

    /**
    * url
    **/
    public void setUrl(String url) {
        this.url = url;
    }

    /**
    * Quick Set snapshot
    * Useful setter to use in builder style (eg. myBinaryRepository.snapshot(Boolean).anotherQuickSetter(..))
    * @param Boolean snapshot
    * @return BinaryRepository The modified BinaryRepository object
    **/
    public BinaryRepository snapshot(Boolean snapshot) {
        this.snapshot = snapshot;
        return this;
    }

    /**
    * snapshot
    * @return Boolean snapshot
    **/
    @ApiModelProperty(value = "snapshot")
    public Boolean isSnapshot() {
        return snapshot;
    }
    public Boolean getSnapshot() {
        return snapshot;
    }

    /**
    * snapshot
    **/
    public void setSnapshot(Boolean snapshot) {
        this.snapshot = snapshot;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BinaryRepository binaryRepository = (BinaryRepository) o;
        return Objects.equals(this.group, binaryRepository.group) &&
        Objects.equals(this.type, binaryRepository.type) &&
        Objects.equals(this.provider, binaryRepository.provider) &&
        Objects.equals(this.providerId, binaryRepository.providerId) &&
        Objects.equals(this.url, binaryRepository.url) &&
        Objects.equals(this.snapshot, binaryRepository.snapshot) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, type, provider, providerId, url, snapshot, super.hashCode());
    }
}

