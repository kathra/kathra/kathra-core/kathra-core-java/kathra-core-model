/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* Membership
*/
@ApiModel(description = "Membership")
public class Membership {

    protected String path = null;
    /**
    * memberType
    */
    public enum MemberTypeEnum {
        USER("user"),
        
        GROUP("group");

        private String value;

        MemberTypeEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static MemberTypeEnum fromValue(String text) {
            for (MemberTypeEnum b : MemberTypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected MemberTypeEnum memberType = null;
    protected String memberName = null;
    /**
    * role
    */
    public enum RoleEnum {
        GUEST("guest"),
        
        CONTRIBUTOR("contributor"),
        
        MANAGER("manager");

        private String value;

        RoleEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static RoleEnum fromValue(String text) {
            for (RoleEnum b : RoleEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected RoleEnum role = null;


    /**
    * Quick Set path
    * Useful setter to use in builder style (eg. myMembership.path(String).anotherQuickSetter(..))
    * @param String path
    * @return Membership The modified Membership object
    **/
    public Membership path(String path) {
        this.path = path;
        return this;
    }

    /**
    * path
    * @return String path
    **/
    @ApiModelProperty(value = "path")
    public String getPath() {
        return path;
    }

    /**
    * path
    **/
    public void setPath(String path) {
        this.path = path;
    }

    /**
    * Quick Set memberType
    * Useful setter to use in builder style (eg. myMembership.memberType(MemberTypeEnum).anotherQuickSetter(..))
    * @param MemberTypeEnum memberType
    * @return Membership The modified Membership object
    **/
    public Membership memberType(MemberTypeEnum memberType) {
        this.memberType = memberType;
        return this;
    }

    /**
    * memberType
    * @return MemberTypeEnum memberType
    **/
    @ApiModelProperty(value = "memberType")
    public MemberTypeEnum getMemberType() {
        return memberType;
    }

    /**
    * memberType
    **/
    public void setMemberType(MemberTypeEnum memberType) {
        this.memberType = memberType;
    }

    /**
    * Quick Set memberName
    * Useful setter to use in builder style (eg. myMembership.memberName(String).anotherQuickSetter(..))
    * @param String memberName
    * @return Membership The modified Membership object
    **/
    public Membership memberName(String memberName) {
        this.memberName = memberName;
        return this;
    }

    /**
    * memberName
    * @return String memberName
    **/
    @ApiModelProperty(value = "memberName")
    public String getMemberName() {
        return memberName;
    }

    /**
    * memberName
    **/
    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    /**
    * Quick Set role
    * Useful setter to use in builder style (eg. myMembership.role(RoleEnum).anotherQuickSetter(..))
    * @param RoleEnum role
    * @return Membership The modified Membership object
    **/
    public Membership role(RoleEnum role) {
        this.role = role;
        return this;
    }

    /**
    * role
    * @return RoleEnum role
    **/
    @ApiModelProperty(value = "role")
    public RoleEnum getRole() {
        return role;
    }

    /**
    * role
    **/
    public void setRole(RoleEnum role) {
        this.role = role;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Membership membership = (Membership) o;
        return Objects.equals(this.path, membership.path) &&
        Objects.equals(this.memberType, membership.memberType) &&
        Objects.equals(this.memberName, membership.memberName) &&
        Objects.equals(this.role, membership.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, memberType, memberName, role);
    }
}

