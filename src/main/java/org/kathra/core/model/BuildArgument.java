/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* BuildArgument
*/
@ApiModel(description = "BuildArgument")
public class BuildArgument {

    protected String key = null;
    protected String value = null;


    /**
    * Quick Set key
    * Useful setter to use in builder style (eg. myBuildArgument.key(String).anotherQuickSetter(..))
    * @param String key
    * @return BuildArgument The modified BuildArgument object
    **/
    public BuildArgument key(String key) {
        this.key = key;
        return this;
    }

    /**
    * Build argument key
    * @return String key
    **/
    @ApiModelProperty(value = "Build argument key")
    public String getKey() {
        return key;
    }

    /**
    * Build argument key
    **/
    public void setKey(String key) {
        this.key = key;
    }

    /**
    * Quick Set value
    * Useful setter to use in builder style (eg. myBuildArgument.value(String).anotherQuickSetter(..))
    * @param String value
    * @return BuildArgument The modified BuildArgument object
    **/
    public BuildArgument value(String value) {
        this.value = value;
        return this;
    }

    /**
    * Build argument value
    * @return String value
    **/
    @ApiModelProperty(value = "Build argument value")
    public String getValue() {
        return value;
    }

    /**
    * Build argument value
    **/
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BuildArgument buildArgument = (BuildArgument) o;
        return Objects.equals(this.key, buildArgument.key) &&
        Objects.equals(this.value, buildArgument.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }
}

