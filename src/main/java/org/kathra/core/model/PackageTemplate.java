/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
* PackageTemplate
*/
@ApiModel(description = "PackageTemplate")
public class PackageTemplate {

    protected String name = null;
    protected List<PackageTemplateArgument> arguments = null;


    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myPackageTemplate.name(String).anotherQuickSetter(..))
    * @param String name
    * @return PackageTemplate The modified PackageTemplate object
    **/
    public PackageTemplate name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Name
    * @return String name
    **/
    @ApiModelProperty(value = "Name")
    public String getName() {
        return name;
    }

    /**
    * Name
    **/
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Quick Set arguments
    * Useful setter to use in builder style (eg. myPackageTemplate.arguments(List<PackageTemplateArgument>).anotherQuickSetter(..))
    * @param List<PackageTemplateArgument> arguments
    * @return PackageTemplate The modified PackageTemplate object
    **/
    public PackageTemplate arguments(List<PackageTemplateArgument> arguments) {
        this.arguments = arguments;
        return this;
    }

    public PackageTemplate addArgumentsItem(PackageTemplateArgument argumentsItem) {
        if (this.arguments == null) {
            this.arguments = new ArrayList();
        }
        this.arguments.add(argumentsItem);
        return this;
    }

    /**
    * Catalog entry arguments
    * @return List<PackageTemplateArgument> arguments
    **/
    @ApiModelProperty(value = "Catalog entry arguments")
    public List<PackageTemplateArgument> getArguments() {
        return arguments;
    }

    /**
    * Catalog entry arguments
    **/
    public void setArguments(List<PackageTemplateArgument> arguments) {
        this.arguments = arguments;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PackageTemplate packageTemplate = (PackageTemplate) o;
        return Objects.equals(this.name, packageTemplate.name) &&
        Objects.equals(this.arguments, packageTemplate.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, arguments);
    }
}

