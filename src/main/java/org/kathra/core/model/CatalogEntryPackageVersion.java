/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
* CatalogEntryPackageVersion
*/
@ApiModel(description = "CatalogEntryPackageVersion")
public class CatalogEntryPackageVersion {

    protected CatalogEntryPackage catalogEntryPackage = null;
    protected String version = null;
    protected String documentation = null;
    protected List<CatalogEntryArgument> arguments = null;


    /**
    * Quick Set catalogEntryPackage
    * Useful setter to use in builder style (eg. myCatalogEntryPackageVersion.catalogEntryPackage(CatalogEntryPackage).anotherQuickSetter(..))
    * @param CatalogEntryPackage catalogEntryPackage
    * @return CatalogEntryPackageVersion The modified CatalogEntryPackageVersion object
    **/
    public CatalogEntryPackageVersion catalogEntryPackage(CatalogEntryPackage catalogEntryPackage) {
        this.catalogEntryPackage = catalogEntryPackage;
        return this;
    }

    /**
    * catalogEntryPackage
    * @return CatalogEntryPackage catalogEntryPackage
    **/
    @ApiModelProperty(value = "catalogEntryPackage")
    public CatalogEntryPackage getCatalogEntryPackage() {
        return catalogEntryPackage;
    }

    /**
    * catalogEntryPackage
    **/
    public void setCatalogEntryPackage(CatalogEntryPackage catalogEntryPackage) {
        this.catalogEntryPackage = catalogEntryPackage;
    }

    /**
    * Quick Set version
    * Useful setter to use in builder style (eg. myCatalogEntryPackageVersion.version(String).anotherQuickSetter(..))
    * @param String version
    * @return CatalogEntryPackageVersion The modified CatalogEntryPackageVersion object
    **/
    public CatalogEntryPackageVersion version(String version) {
        this.version = version;
        return this;
    }

    /**
    * Version
    * @return String version
    **/
    @ApiModelProperty(value = "Version")
    public String getVersion() {
        return version;
    }

    /**
    * Version
    **/
    public void setVersion(String version) {
        this.version = version;
    }

    /**
    * Quick Set documentation
    * Useful setter to use in builder style (eg. myCatalogEntryPackageVersion.documentation(String).anotherQuickSetter(..))
    * @param String documentation
    * @return CatalogEntryPackageVersion The modified CatalogEntryPackageVersion object
    **/
    public CatalogEntryPackageVersion documentation(String documentation) {
        this.documentation = documentation;
        return this;
    }

    /**
    * Documentation of catalog entry (Markdown)
    * @return String documentation
    **/
    @ApiModelProperty(value = "Documentation of catalog entry (Markdown)")
    public String getDocumentation() {
        return documentation;
    }

    /**
    * Documentation of catalog entry (Markdown)
    **/
    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    /**
    * Quick Set arguments
    * Useful setter to use in builder style (eg. myCatalogEntryPackageVersion.arguments(List<CatalogEntryArgument>).anotherQuickSetter(..))
    * @param List<CatalogEntryArgument> arguments
    * @return CatalogEntryPackageVersion The modified CatalogEntryPackageVersion object
    **/
    public CatalogEntryPackageVersion arguments(List<CatalogEntryArgument> arguments) {
        this.arguments = arguments;
        return this;
    }

    public CatalogEntryPackageVersion addArgumentsItem(CatalogEntryArgument argumentsItem) {
        if (this.arguments == null) {
            this.arguments = new ArrayList();
        }
        this.arguments.add(argumentsItem);
        return this;
    }

    /**
    * Catalog entry arguments
    * @return List<CatalogEntryArgument> arguments
    **/
    @ApiModelProperty(value = "Catalog entry arguments")
    public List<CatalogEntryArgument> getArguments() {
        return arguments;
    }

    /**
    * Catalog entry arguments
    **/
    public void setArguments(List<CatalogEntryArgument> arguments) {
        this.arguments = arguments;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CatalogEntryPackageVersion catalogEntryPackageVersion = (CatalogEntryPackageVersion) o;
        return Objects.equals(this.catalogEntryPackage, catalogEntryPackageVersion.catalogEntryPackage) &&
        Objects.equals(this.version, catalogEntryPackageVersion.version) &&
        Objects.equals(this.documentation, catalogEntryPackageVersion.documentation) &&
        Objects.equals(this.arguments, catalogEntryPackageVersion.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(catalogEntryPackage, version, documentation, arguments);
    }
}

