/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* PackageTemplateArgument
*/
@ApiModel(description = "PackageTemplateArgument")
public class PackageTemplateArgument {

    protected String key = null;
    protected String value = null;
    protected String contrainst = null;


    /**
    * Quick Set key
    * Useful setter to use in builder style (eg. myPackageTemplateArgument.key(String).anotherQuickSetter(..))
    * @param String key
    * @return PackageTemplateArgument The modified PackageTemplateArgument object
    **/
    public PackageTemplateArgument key(String key) {
        this.key = key;
        return this;
    }

    /**
    * Argument key to generate catalog entry
    * @return String key
    **/
    @ApiModelProperty(value = "Argument key to generate catalog entry")
    public String getKey() {
        return key;
    }

    /**
    * Argument key to generate catalog entry
    **/
    public void setKey(String key) {
        this.key = key;
    }

    /**
    * Quick Set value
    * Useful setter to use in builder style (eg. myPackageTemplateArgument.value(String).anotherQuickSetter(..))
    * @param String value
    * @return PackageTemplateArgument The modified PackageTemplateArgument object
    **/
    public PackageTemplateArgument value(String value) {
        this.value = value;
        return this;
    }

    /**
    * Argument value to generate catalog entry
    * @return String value
    **/
    @ApiModelProperty(value = "Argument value to generate catalog entry")
    public String getValue() {
        return value;
    }

    /**
    * Argument value to generate catalog entry
    **/
    public void setValue(String value) {
        this.value = value;
    }

    /**
    * Quick Set contrainst
    * Useful setter to use in builder style (eg. myPackageTemplateArgument.contrainst(String).anotherQuickSetter(..))
    * @param String contrainst
    * @return PackageTemplateArgument The modified PackageTemplateArgument object
    **/
    public PackageTemplateArgument contrainst(String contrainst) {
        this.contrainst = contrainst;
        return this;
    }

    /**
    * Argument constraint
    * @return String contrainst
    **/
    @ApiModelProperty(value = "Argument constraint")
    public String getContrainst() {
        return contrainst;
    }

    /**
    * Argument constraint
    **/
    public void setContrainst(String contrainst) {
        this.contrainst = contrainst;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PackageTemplateArgument packageTemplateArgument = (PackageTemplateArgument) o;
        return Objects.equals(this.key, packageTemplateArgument.key) &&
        Objects.equals(this.value, packageTemplateArgument.value) &&
        Objects.equals(this.contrainst, packageTemplateArgument.contrainst);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value, contrainst);
    }
}

