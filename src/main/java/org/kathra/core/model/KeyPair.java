/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import java.util.Objects;

/**
* KeyPair
*/
@ApiModel(description = "KeyPair")
public class KeyPair extends Resource {

    protected String privateKey = null;
    protected String publicKey = null;
    protected Group group = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myKeyPair.id(String).anotherQuickSetter(..))
    * @param String id
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myKeyPair.name(String).anotherQuickSetter(..))
    * @param String name
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myKeyPair.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myKeyPair.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myKeyPair.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myKeyPair.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myKeyPair.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myKeyPair.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public KeyPair putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set privateKey
    * Useful setter to use in builder style (eg. myKeyPair.privateKey(String).anotherQuickSetter(..))
    * @param String privateKey
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair privateKey(String privateKey) {
        this.privateKey = privateKey;
        return this;
    }

    /**
    * privateKey
    * @return String privateKey
    **/
    @ApiModelProperty(value = "privateKey")
    public String getPrivateKey() {
        return privateKey;
    }

    /**
    * privateKey
    **/
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    /**
    * Quick Set publicKey
    * Useful setter to use in builder style (eg. myKeyPair.publicKey(String).anotherQuickSetter(..))
    * @param String publicKey
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair publicKey(String publicKey) {
        this.publicKey = publicKey;
        return this;
    }

    /**
    * publicKey
    * @return String publicKey
    **/
    @ApiModelProperty(value = "publicKey")
    public String getPublicKey() {
        return publicKey;
    }

    /**
    * publicKey
    **/
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    /**
    * Quick Set group
    * Useful setter to use in builder style (eg. myKeyPair.group(Group).anotherQuickSetter(..))
    * @param Group group
    * @return KeyPair The modified KeyPair object
    **/
    public KeyPair group(Group group) {
        this.group = group;
        return this;
    }

    /**
    * group
    * @return Group group
    **/
    @ApiModelProperty(value = "group")
    public Group getGroup() {
        return group;
    }

    /**
    * group
    **/
    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KeyPair keyPair = (KeyPair) o;
        return Objects.equals(this.privateKey, keyPair.privateKey) &&
        Objects.equals(this.publicKey, keyPair.publicKey) &&
        Objects.equals(this.group, keyPair.group) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(privateKey, publicKey, group, super.hashCode());
    }
}

