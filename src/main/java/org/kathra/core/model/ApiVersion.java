/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* ApiVersion
*/
@ApiModel(description = "ApiVersion")
public class ApiVersion extends Resource {

    protected Component component = null;
    protected Boolean released = null;
    protected String version = null;
    /**
    * apiRepositoryStatus
    */
    public enum ApiRepositoryStatusEnum {
        PENDING("PENDING"),
        
        READY("READY"),
        
        UPDATING("UPDATING"),
        
        UNSTABLE("UNSTABLE"),
        
        ERROR("ERROR"),
        
        DELETED("DELETED");

        private String value;

        ApiRepositoryStatusEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static ApiRepositoryStatusEnum fromValue(String text) {
            for (ApiRepositoryStatusEnum b : ApiRepositoryStatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected ApiRepositoryStatusEnum apiRepositoryStatus = null;
    protected List<LibraryApiVersion> librariesApiVersions = null;
    protected List<ImplementationVersion> implementationsVersions = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myApiVersion.id(String).anotherQuickSetter(..))
    * @param String id
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myApiVersion.name(String).anotherQuickSetter(..))
    * @param String name
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myApiVersion.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myApiVersion.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myApiVersion.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myApiVersion.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myApiVersion.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myApiVersion.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public ApiVersion putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set component
    * Useful setter to use in builder style (eg. myApiVersion.component(Component).anotherQuickSetter(..))
    * @param Component component
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion component(Component component) {
        this.component = component;
        return this;
    }

    /**
    * component
    * @return Component component
    **/
    @ApiModelProperty(value = "component")
    public Component getComponent() {
        return component;
    }

    /**
    * component
    **/
    public void setComponent(Component component) {
        this.component = component;
    }

    /**
    * Quick Set released
    * Useful setter to use in builder style (eg. myApiVersion.released(Boolean).anotherQuickSetter(..))
    * @param Boolean released
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion released(Boolean released) {
        this.released = released;
        return this;
    }

    /**
    * released
    * @return Boolean released
    **/
    @ApiModelProperty(value = "released")
    public Boolean isReleased() {
        return released;
    }
    public Boolean getReleased() {
        return released;
    }

    /**
    * released
    **/
    public void setReleased(Boolean released) {
        this.released = released;
    }

    /**
    * Quick Set version
    * Useful setter to use in builder style (eg. myApiVersion.version(String).anotherQuickSetter(..))
    * @param String version
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion version(String version) {
        this.version = version;
        return this;
    }

    /**
    * version
    * @return String version
    **/
    @ApiModelProperty(value = "version")
    public String getVersion() {
        return version;
    }

    /**
    * version
    **/
    public void setVersion(String version) {
        this.version = version;
    }

    /**
    * Quick Set apiRepositoryStatus
    * Useful setter to use in builder style (eg. myApiVersion.apiRepositoryStatus(ApiRepositoryStatusEnum).anotherQuickSetter(..))
    * @param ApiRepositoryStatusEnum apiRepositoryStatus
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion apiRepositoryStatus(ApiRepositoryStatusEnum apiRepositoryStatus) {
        this.apiRepositoryStatus = apiRepositoryStatus;
        return this;
    }

    /**
    * apiRepositoryStatus
    * @return ApiRepositoryStatusEnum apiRepositoryStatus
    **/
    @ApiModelProperty(value = "apiRepositoryStatus")
    public ApiRepositoryStatusEnum getApiRepositoryStatus() {
        return apiRepositoryStatus;
    }

    /**
    * apiRepositoryStatus
    **/
    public void setApiRepositoryStatus(ApiRepositoryStatusEnum apiRepositoryStatus) {
        this.apiRepositoryStatus = apiRepositoryStatus;
    }

    /**
    * Quick Set librariesApiVersions
    * Useful setter to use in builder style (eg. myApiVersion.librariesApiVersions(List<LibraryApiVersion>).anotherQuickSetter(..))
    * @param List<LibraryApiVersion> librariesApiVersions
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion librariesApiVersions(List<LibraryApiVersion> librariesApiVersions) {
        this.librariesApiVersions = librariesApiVersions;
        return this;
    }

    public ApiVersion addLibrariesApiVersionsItem(LibraryApiVersion librariesApiVersionsItem) {
        if (this.librariesApiVersions == null) {
            this.librariesApiVersions = new ArrayList();
        }
        this.librariesApiVersions.add(librariesApiVersionsItem);
        return this;
    }

    /**
    * librariesApiVersions
    * @return List<LibraryApiVersion> librariesApiVersions
    **/
    @ApiModelProperty(value = "librariesApiVersions")
    public List<LibraryApiVersion> getLibrariesApiVersions() {
        return librariesApiVersions;
    }

    /**
    * librariesApiVersions
    **/
    public void setLibrariesApiVersions(List<LibraryApiVersion> librariesApiVersions) {
        this.librariesApiVersions = librariesApiVersions;
    }

    /**
    * Quick Set implementationsVersions
    * Useful setter to use in builder style (eg. myApiVersion.implementationsVersions(List<ImplementationVersion>).anotherQuickSetter(..))
    * @param List<ImplementationVersion> implementationsVersions
    * @return ApiVersion The modified ApiVersion object
    **/
    public ApiVersion implementationsVersions(List<ImplementationVersion> implementationsVersions) {
        this.implementationsVersions = implementationsVersions;
        return this;
    }

    public ApiVersion addImplementationsVersionsItem(ImplementationVersion implementationsVersionsItem) {
        if (this.implementationsVersions == null) {
            this.implementationsVersions = new ArrayList();
        }
        this.implementationsVersions.add(implementationsVersionsItem);
        return this;
    }

    /**
    * implementationsVersions
    * @return List<ImplementationVersion> implementationsVersions
    **/
    @ApiModelProperty(value = "implementationsVersions")
    public List<ImplementationVersion> getImplementationsVersions() {
        return implementationsVersions;
    }

    /**
    * implementationsVersions
    **/
    public void setImplementationsVersions(List<ImplementationVersion> implementationsVersions) {
        this.implementationsVersions = implementationsVersions;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ApiVersion apiVersion = (ApiVersion) o;
        return Objects.equals(this.component, apiVersion.component) &&
        Objects.equals(this.released, apiVersion.released) &&
        Objects.equals(this.version, apiVersion.version) &&
        Objects.equals(this.apiRepositoryStatus, apiVersion.apiRepositoryStatus) &&
        Objects.equals(this.librariesApiVersions, apiVersion.librariesApiVersions) &&
        Objects.equals(this.implementationsVersions, apiVersion.implementationsVersions) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(component, released, version, apiRepositoryStatus, librariesApiVersions, implementationsVersions, super.hashCode());
    }
}

