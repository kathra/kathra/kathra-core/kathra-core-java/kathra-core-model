/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* CatalogEntryArgument
*/
@ApiModel(description = "CatalogEntryArgument")
public class CatalogEntryArgument {

    protected String label = null;
    protected String description = null;
    protected String key = null;
    protected String value = null;
    protected String contrainst = null;


    /**
    * Quick Set label
    * Useful setter to use in builder style (eg. myCatalogEntryArgument.label(String).anotherQuickSetter(..))
    * @param String label
    * @return CatalogEntryArgument The modified CatalogEntryArgument object
    **/
    public CatalogEntryArgument label(String label) {
        this.label = label;
        return this;
    }

    /**
    * Readable argument for catalog entry
    * @return String label
    **/
    @ApiModelProperty(value = "Readable argument for catalog entry")
    public String getLabel() {
        return label;
    }

    /**
    * Readable argument for catalog entry
    **/
    public void setLabel(String label) {
        this.label = label;
    }

    /**
    * Quick Set description
    * Useful setter to use in builder style (eg. myCatalogEntryArgument.description(String).anotherQuickSetter(..))
    * @param String description
    * @return CatalogEntryArgument The modified CatalogEntryArgument object
    **/
    public CatalogEntryArgument description(String description) {
        this.description = description;
        return this;
    }

    /**
    * Description argument for catalog entry
    * @return String description
    **/
    @ApiModelProperty(value = "Description argument for catalog entry")
    public String getDescription() {
        return description;
    }

    /**
    * Description argument for catalog entry
    **/
    public void setDescription(String description) {
        this.description = description;
    }

    /**
    * Quick Set key
    * Useful setter to use in builder style (eg. myCatalogEntryArgument.key(String).anotherQuickSetter(..))
    * @param String key
    * @return CatalogEntryArgument The modified CatalogEntryArgument object
    **/
    public CatalogEntryArgument key(String key) {
        this.key = key;
        return this;
    }

    /**
    * Argument key for catalog entry
    * @return String key
    **/
    @ApiModelProperty(value = "Argument key for catalog entry")
    public String getKey() {
        return key;
    }

    /**
    * Argument key for catalog entry
    **/
    public void setKey(String key) {
        this.key = key;
    }

    /**
    * Quick Set value
    * Useful setter to use in builder style (eg. myCatalogEntryArgument.value(String).anotherQuickSetter(..))
    * @param String value
    * @return CatalogEntryArgument The modified CatalogEntryArgument object
    **/
    public CatalogEntryArgument value(String value) {
        this.value = value;
        return this;
    }

    /**
    * Argument value for catalog entry
    * @return String value
    **/
    @ApiModelProperty(value = "Argument value for catalog entry")
    public String getValue() {
        return value;
    }

    /**
    * Argument value for catalog entry
    **/
    public void setValue(String value) {
        this.value = value;
    }

    /**
    * Quick Set contrainst
    * Useful setter to use in builder style (eg. myCatalogEntryArgument.contrainst(String).anotherQuickSetter(..))
    * @param String contrainst
    * @return CatalogEntryArgument The modified CatalogEntryArgument object
    **/
    public CatalogEntryArgument contrainst(String contrainst) {
        this.contrainst = contrainst;
        return this;
    }

    /**
    * Argument constraint for catalog entry
    * @return String contrainst
    **/
    @ApiModelProperty(value = "Argument constraint for catalog entry")
    public String getContrainst() {
        return contrainst;
    }

    /**
    * Argument constraint for catalog entry
    **/
    public void setContrainst(String contrainst) {
        this.contrainst = contrainst;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CatalogEntryArgument catalogEntryArgument = (CatalogEntryArgument) o;
        return Objects.equals(this.label, catalogEntryArgument.label) &&
        Objects.equals(this.description, catalogEntryArgument.description) &&
        Objects.equals(this.key, catalogEntryArgument.key) &&
        Objects.equals(this.value, catalogEntryArgument.value) &&
        Objects.equals(this.contrainst, catalogEntryArgument.contrainst);
    }

    @Override
    public int hashCode() {
        return Objects.hash(label, description, key, value, contrainst);
    }
}

