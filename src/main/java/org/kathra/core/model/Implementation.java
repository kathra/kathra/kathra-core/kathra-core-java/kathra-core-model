/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* Implementation
*/
@ApiModel(description = "Implementation")
public class Implementation extends Asset {

    protected Component component = null;
    protected String description = null;
    /**
    * language
    */
    public enum LanguageEnum {
        JAVA("JAVA"),
        
        PYTHON("PYTHON");

        private String value;

        LanguageEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static LanguageEnum fromValue(String text) {
            for (LanguageEnum b : LanguageEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected LanguageEnum language = null;
    protected String title = null;
    protected List<ImplementationVersion> versions = null;
    protected List<CatalogEntry> catalogEntries = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myImplementation.id(String).anotherQuickSetter(..))
    * @param String id
    * @return Implementation The modified Implementation object
    **/
    public Implementation id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myImplementation.name(String).anotherQuickSetter(..))
    * @param String name
    * @return Implementation The modified Implementation object
    **/
    public Implementation name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myImplementation.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return Implementation The modified Implementation object
    **/
    public Implementation status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myImplementation.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return Implementation The modified Implementation object
    **/
    public Implementation createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myImplementation.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return Implementation The modified Implementation object
    **/
    public Implementation updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myImplementation.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return Implementation The modified Implementation object
    **/
    public Implementation createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myImplementation.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return Implementation The modified Implementation object
    **/
    public Implementation updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myImplementation.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return Implementation The modified Implementation object
    **/
    public Implementation metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public Implementation putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }
    /**
    * Quick Set binaryRepository
    * Useful setter to use in builder style (eg. myImplementation.binaryRepository(BinaryRepository).anotherQuickSetter(..))
    * @param BinaryRepository binaryRepository
    * @return Implementation The modified Implementation object
    **/
    public Implementation binaryRepository(BinaryRepository binaryRepository) {
        this.binaryRepository = binaryRepository;
        return this;
    }

    /**
    * Quick Set sourceRepository
    * Useful setter to use in builder style (eg. myImplementation.sourceRepository(SourceRepository).anotherQuickSetter(..))
    * @param SourceRepository sourceRepository
    * @return Implementation The modified Implementation object
    **/
    public Implementation sourceRepository(SourceRepository sourceRepository) {
        this.sourceRepository = sourceRepository;
        return this;
    }

    /**
    * Quick Set pipeline
    * Useful setter to use in builder style (eg. myImplementation.pipeline(Pipeline).anotherQuickSetter(..))
    * @param Pipeline pipeline
    * @return Implementation The modified Implementation object
    **/
    public Implementation pipeline(Pipeline pipeline) {
        this.pipeline = pipeline;
        return this;
    }


    /**
    * Quick Set component
    * Useful setter to use in builder style (eg. myImplementation.component(Component).anotherQuickSetter(..))
    * @param Component component
    * @return Implementation The modified Implementation object
    **/
    public Implementation component(Component component) {
        this.component = component;
        return this;
    }

    /**
    * component
    * @return Component component
    **/
    @ApiModelProperty(value = "component")
    public Component getComponent() {
        return component;
    }

    /**
    * component
    **/
    public void setComponent(Component component) {
        this.component = component;
    }

    /**
    * Quick Set description
    * Useful setter to use in builder style (eg. myImplementation.description(String).anotherQuickSetter(..))
    * @param String description
    * @return Implementation The modified Implementation object
    **/
    public Implementation description(String description) {
        this.description = description;
        return this;
    }

    /**
    * description
    * @return String description
    **/
    @ApiModelProperty(value = "description")
    public String getDescription() {
        return description;
    }

    /**
    * description
    **/
    public void setDescription(String description) {
        this.description = description;
    }

    /**
    * Quick Set language
    * Useful setter to use in builder style (eg. myImplementation.language(LanguageEnum).anotherQuickSetter(..))
    * @param LanguageEnum language
    * @return Implementation The modified Implementation object
    **/
    public Implementation language(LanguageEnum language) {
        this.language = language;
        return this;
    }

    /**
    * language
    * @return LanguageEnum language
    **/
    @ApiModelProperty(value = "language")
    public LanguageEnum getLanguage() {
        return language;
    }

    /**
    * language
    **/
    public void setLanguage(LanguageEnum language) {
        this.language = language;
    }

    /**
    * Quick Set title
    * Useful setter to use in builder style (eg. myImplementation.title(String).anotherQuickSetter(..))
    * @param String title
    * @return Implementation The modified Implementation object
    **/
    public Implementation title(String title) {
        this.title = title;
        return this;
    }

    /**
    * title
    * @return String title
    **/
    @ApiModelProperty(value = "title")
    public String getTitle() {
        return title;
    }

    /**
    * title
    **/
    public void setTitle(String title) {
        this.title = title;
    }

    /**
    * Quick Set versions
    * Useful setter to use in builder style (eg. myImplementation.versions(List<ImplementationVersion>).anotherQuickSetter(..))
    * @param List<ImplementationVersion> versions
    * @return Implementation The modified Implementation object
    **/
    public Implementation versions(List<ImplementationVersion> versions) {
        this.versions = versions;
        return this;
    }

    public Implementation addVersionsItem(ImplementationVersion versionsItem) {
        if (this.versions == null) {
            this.versions = new ArrayList();
        }
        this.versions.add(versionsItem);
        return this;
    }

    /**
    * versions
    * @return List<ImplementationVersion> versions
    **/
    @ApiModelProperty(value = "versions")
    public List<ImplementationVersion> getVersions() {
        return versions;
    }

    /**
    * versions
    **/
    public void setVersions(List<ImplementationVersion> versions) {
        this.versions = versions;
    }

    /**
    * Quick Set catalogEntries
    * Useful setter to use in builder style (eg. myImplementation.catalogEntries(List<CatalogEntry>).anotherQuickSetter(..))
    * @param List<CatalogEntry> catalogEntries
    * @return Implementation The modified Implementation object
    **/
    public Implementation catalogEntries(List<CatalogEntry> catalogEntries) {
        this.catalogEntries = catalogEntries;
        return this;
    }

    public Implementation addCatalogEntriesItem(CatalogEntry catalogEntriesItem) {
        if (this.catalogEntries == null) {
            this.catalogEntries = new ArrayList();
        }
        this.catalogEntries.add(catalogEntriesItem);
        return this;
    }

    /**
    * catalogEntries
    * @return List<CatalogEntry> catalogEntries
    **/
    @ApiModelProperty(value = "catalogEntries")
    public List<CatalogEntry> getCatalogEntries() {
        return catalogEntries;
    }

    /**
    * catalogEntries
    **/
    public void setCatalogEntries(List<CatalogEntry> catalogEntries) {
        this.catalogEntries = catalogEntries;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Implementation implementation = (Implementation) o;
        return Objects.equals(this.component, implementation.component) &&
        Objects.equals(this.description, implementation.description) &&
        Objects.equals(this.language, implementation.language) &&
        Objects.equals(this.title, implementation.title) &&
        Objects.equals(this.versions, implementation.versions) &&
        Objects.equals(this.catalogEntries, implementation.catalogEntries) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(component, description, language, title, versions, catalogEntries, super.hashCode());
    }
}

