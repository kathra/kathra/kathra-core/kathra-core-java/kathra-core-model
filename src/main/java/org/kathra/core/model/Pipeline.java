/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import java.util.Objects;

/**
* Pipeline
*/
@ApiModel(description = "Pipeline")
public class Pipeline extends Resource {

    protected String provider = null;
    protected String providerId = null;
    protected String credentialId = null;
    protected String path = null;
    protected SourceRepository sourceRepository = null;
    /**
    * template
    */
    public enum TemplateEnum {
        JAVA_LIBRARY("JAVA_LIBRARY"),
        
        PYTHON_LIBRARY("PYTHON_LIBRARY"),
        
        JAVA_SERVICE("JAVA_SERVICE"),
        
        PYTHON_SERVICE("PYTHON_SERVICE"),
        
        HELM_PACKAGE("HELM_PACKAGE"),
        
        DOCKER_SERVICE("DOCKER_SERVICE");

        private String value;

        TemplateEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static TemplateEnum fromValue(String text) {
            for (TemplateEnum b : TemplateEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected TemplateEnum template = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myPipeline.id(String).anotherQuickSetter(..))
    * @param String id
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myPipeline.name(String).anotherQuickSetter(..))
    * @param String name
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myPipeline.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myPipeline.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myPipeline.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myPipeline.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myPipeline.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myPipeline.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public Pipeline putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set provider
    * Useful setter to use in builder style (eg. myPipeline.provider(String).anotherQuickSetter(..))
    * @param String provider
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline provider(String provider) {
        this.provider = provider;
        return this;
    }

    /**
    * provider
    * @return String provider
    **/
    @ApiModelProperty(value = "provider")
    public String getProvider() {
        return provider;
    }

    /**
    * provider
    **/
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
    * Quick Set providerId
    * Useful setter to use in builder style (eg. myPipeline.providerId(String).anotherQuickSetter(..))
    * @param String providerId
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline providerId(String providerId) {
        this.providerId = providerId;
        return this;
    }

    /**
    * providerId
    * @return String providerId
    **/
    @ApiModelProperty(value = "providerId")
    public String getProviderId() {
        return providerId;
    }

    /**
    * providerId
    **/
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    /**
    * Quick Set credentialId
    * Useful setter to use in builder style (eg. myPipeline.credentialId(String).anotherQuickSetter(..))
    * @param String credentialId
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline credentialId(String credentialId) {
        this.credentialId = credentialId;
        return this;
    }

    /**
    * credentialId
    * @return String credentialId
    **/
    @ApiModelProperty(value = "credentialId")
    public String getCredentialId() {
        return credentialId;
    }

    /**
    * credentialId
    **/
    public void setCredentialId(String credentialId) {
        this.credentialId = credentialId;
    }

    /**
    * Quick Set path
    * Useful setter to use in builder style (eg. myPipeline.path(String).anotherQuickSetter(..))
    * @param String path
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline path(String path) {
        this.path = path;
        return this;
    }

    /**
    * path
    * @return String path
    **/
    @ApiModelProperty(value = "path")
    public String getPath() {
        return path;
    }

    /**
    * path
    **/
    public void setPath(String path) {
        this.path = path;
    }

    /**
    * Quick Set sourceRepository
    * Useful setter to use in builder style (eg. myPipeline.sourceRepository(SourceRepository).anotherQuickSetter(..))
    * @param SourceRepository sourceRepository
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline sourceRepository(SourceRepository sourceRepository) {
        this.sourceRepository = sourceRepository;
        return this;
    }

    /**
    * sourceRepository
    * @return SourceRepository sourceRepository
    **/
    @ApiModelProperty(value = "sourceRepository")
    public SourceRepository getSourceRepository() {
        return sourceRepository;
    }

    /**
    * sourceRepository
    **/
    public void setSourceRepository(SourceRepository sourceRepository) {
        this.sourceRepository = sourceRepository;
    }

    /**
    * Quick Set template
    * Useful setter to use in builder style (eg. myPipeline.template(TemplateEnum).anotherQuickSetter(..))
    * @param TemplateEnum template
    * @return Pipeline The modified Pipeline object
    **/
    public Pipeline template(TemplateEnum template) {
        this.template = template;
        return this;
    }

    /**
    * template
    * @return TemplateEnum template
    **/
    @ApiModelProperty(value = "template")
    public TemplateEnum getTemplate() {
        return template;
    }

    /**
    * template
    **/
    public void setTemplate(TemplateEnum template) {
        this.template = template;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pipeline pipeline = (Pipeline) o;
        return Objects.equals(this.provider, pipeline.provider) &&
        Objects.equals(this.providerId, pipeline.providerId) &&
        Objects.equals(this.credentialId, pipeline.credentialId) &&
        Objects.equals(this.path, pipeline.path) &&
        Objects.equals(this.sourceRepository, pipeline.sourceRepository) &&
        Objects.equals(this.template, pipeline.template) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(provider, providerId, credentialId, path, sourceRepository, template, super.hashCode());
    }
}

