/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* Group
*/
@ApiModel(description = "Group")
public class Group extends Resource {

    protected String path = null;
    protected User technicalUser = null;
    protected List<BinaryRepository> binaryRepositories = null;
    protected List<Assignation> members = null;
    protected Group parent = null;
    /**
    * sourceRepositoryStatus
    */
    public enum SourceRepositoryStatusEnum {
        PENDING("PENDING"),
        
        READY("READY"),
        
        UPDATING("UPDATING"),
        
        UNSTABLE("UNSTABLE"),
        
        ERROR("ERROR"),
        
        DELETED("DELETED");

        private String value;

        SourceRepositoryStatusEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static SourceRepositoryStatusEnum fromValue(String text) {
            for (SourceRepositoryStatusEnum b : SourceRepositoryStatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected SourceRepositoryStatusEnum sourceRepositoryStatus = null;
    /**
    * sourceMembershipStatus
    */
    public enum SourceMembershipStatusEnum {
        PENDING("PENDING"),
        
        READY("READY"),
        
        UPDATING("UPDATING"),
        
        UNSTABLE("UNSTABLE"),
        
        ERROR("ERROR"),
        
        DELETED("DELETED");

        private String value;

        SourceMembershipStatusEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static SourceMembershipStatusEnum fromValue(String text) {
            for (SourceMembershipStatusEnum b : SourceMembershipStatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected SourceMembershipStatusEnum sourceMembershipStatus = null;
    /**
    * pipelineFolderStatus
    */
    public enum PipelineFolderStatusEnum {
        PENDING("PENDING"),
        
        READY("READY"),
        
        UPDATING("UPDATING"),
        
        UNSTABLE("UNSTABLE"),
        
        ERROR("ERROR"),
        
        DELETED("DELETED");

        private String value;

        PipelineFolderStatusEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static PipelineFolderStatusEnum fromValue(String text) {
            for (PipelineFolderStatusEnum b : PipelineFolderStatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected PipelineFolderStatusEnum pipelineFolderStatus = null;
    /**
    * binaryRepositoryStatus
    */
    public enum BinaryRepositoryStatusEnum {
        PENDING("PENDING"),
        
        READY("READY"),
        
        UPDATING("UPDATING"),
        
        UNSTABLE("UNSTABLE"),
        
        ERROR("ERROR"),
        
        DELETED("DELETED");

        private String value;

        BinaryRepositoryStatusEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static BinaryRepositoryStatusEnum fromValue(String text) {
            for (BinaryRepositoryStatusEnum b : BinaryRepositoryStatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected BinaryRepositoryStatusEnum binaryRepositoryStatus = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myGroup.id(String).anotherQuickSetter(..))
    * @param String id
    * @return Group The modified Group object
    **/
    public Group id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myGroup.name(String).anotherQuickSetter(..))
    * @param String name
    * @return Group The modified Group object
    **/
    public Group name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myGroup.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return Group The modified Group object
    **/
    public Group status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myGroup.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return Group The modified Group object
    **/
    public Group createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myGroup.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return Group The modified Group object
    **/
    public Group updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myGroup.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return Group The modified Group object
    **/
    public Group createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myGroup.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return Group The modified Group object
    **/
    public Group updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myGroup.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return Group The modified Group object
    **/
    public Group metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public Group putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set path
    * Useful setter to use in builder style (eg. myGroup.path(String).anotherQuickSetter(..))
    * @param String path
    * @return Group The modified Group object
    **/
    public Group path(String path) {
        this.path = path;
        return this;
    }

    /**
    * Group's path in case of subgroup
    * @return String path
    **/
    @ApiModelProperty(value = "Group's path in case of subgroup")
    public String getPath() {
        return path;
    }

    /**
    * Group's path in case of subgroup
    **/
    public void setPath(String path) {
        this.path = path;
    }

    /**
    * Quick Set technicalUser
    * Useful setter to use in builder style (eg. myGroup.technicalUser(User).anotherQuickSetter(..))
    * @param User technicalUser
    * @return Group The modified Group object
    **/
    public Group technicalUser(User technicalUser) {
        this.technicalUser = technicalUser;
        return this;
    }

    /**
    * technicalUser
    * @return User technicalUser
    **/
    @ApiModelProperty(value = "technicalUser")
    public User getTechnicalUser() {
        return technicalUser;
    }

    /**
    * technicalUser
    **/
    public void setTechnicalUser(User technicalUser) {
        this.technicalUser = technicalUser;
    }

    /**
    * Quick Set binaryRepositories
    * Useful setter to use in builder style (eg. myGroup.binaryRepositories(List<BinaryRepository>).anotherQuickSetter(..))
    * @param List<BinaryRepository> binaryRepositories
    * @return Group The modified Group object
    **/
    public Group binaryRepositories(List<BinaryRepository> binaryRepositories) {
        this.binaryRepositories = binaryRepositories;
        return this;
    }

    public Group addBinaryRepositoriesItem(BinaryRepository binaryRepositoriesItem) {
        if (this.binaryRepositories == null) {
            this.binaryRepositories = new ArrayList();
        }
        this.binaryRepositories.add(binaryRepositoriesItem);
        return this;
    }

    /**
    * binaryRepositories
    * @return List<BinaryRepository> binaryRepositories
    **/
    @ApiModelProperty(value = "binaryRepositories")
    public List<BinaryRepository> getBinaryRepositories() {
        return binaryRepositories;
    }

    /**
    * binaryRepositories
    **/
    public void setBinaryRepositories(List<BinaryRepository> binaryRepositories) {
        this.binaryRepositories = binaryRepositories;
    }

    /**
    * Quick Set members
    * Useful setter to use in builder style (eg. myGroup.members(List<Assignation>).anotherQuickSetter(..))
    * @param List<Assignation> members
    * @return Group The modified Group object
    **/
    public Group members(List<Assignation> members) {
        this.members = members;
        return this;
    }

    public Group addMembersItem(Assignation membersItem) {
        if (this.members == null) {
            this.members = new ArrayList();
        }
        this.members.add(membersItem);
        return this;
    }

    /**
    * members
    * @return List<Assignation> members
    **/
    @ApiModelProperty(value = "members")
    public List<Assignation> getMembers() {
        return members;
    }

    /**
    * members
    **/
    public void setMembers(List<Assignation> members) {
        this.members = members;
    }

    /**
    * Quick Set parent
    * Useful setter to use in builder style (eg. myGroup.parent(Group).anotherQuickSetter(..))
    * @param Group parent
    * @return Group The modified Group object
    **/
    public Group parent(Group parent) {
        this.parent = parent;
        return this;
    }

    /**
    * parent
    * @return Group parent
    **/
    @ApiModelProperty(value = "parent")
    public Group getParent() {
        return parent;
    }

    /**
    * parent
    **/
    public void setParent(Group parent) {
        this.parent = parent;
    }

    /**
    * Quick Set sourceRepositoryStatus
    * Useful setter to use in builder style (eg. myGroup.sourceRepositoryStatus(SourceRepositoryStatusEnum).anotherQuickSetter(..))
    * @param SourceRepositoryStatusEnum sourceRepositoryStatus
    * @return Group The modified Group object
    **/
    public Group sourceRepositoryStatus(SourceRepositoryStatusEnum sourceRepositoryStatus) {
        this.sourceRepositoryStatus = sourceRepositoryStatus;
        return this;
    }

    /**
    * sourceRepositoryStatus
    * @return SourceRepositoryStatusEnum sourceRepositoryStatus
    **/
    @ApiModelProperty(value = "sourceRepositoryStatus")
    public SourceRepositoryStatusEnum getSourceRepositoryStatus() {
        return sourceRepositoryStatus;
    }

    /**
    * sourceRepositoryStatus
    **/
    public void setSourceRepositoryStatus(SourceRepositoryStatusEnum sourceRepositoryStatus) {
        this.sourceRepositoryStatus = sourceRepositoryStatus;
    }

    /**
    * Quick Set sourceMembershipStatus
    * Useful setter to use in builder style (eg. myGroup.sourceMembershipStatus(SourceMembershipStatusEnum).anotherQuickSetter(..))
    * @param SourceMembershipStatusEnum sourceMembershipStatus
    * @return Group The modified Group object
    **/
    public Group sourceMembershipStatus(SourceMembershipStatusEnum sourceMembershipStatus) {
        this.sourceMembershipStatus = sourceMembershipStatus;
        return this;
    }

    /**
    * sourceMembershipStatus
    * @return SourceMembershipStatusEnum sourceMembershipStatus
    **/
    @ApiModelProperty(value = "sourceMembershipStatus")
    public SourceMembershipStatusEnum getSourceMembershipStatus() {
        return sourceMembershipStatus;
    }

    /**
    * sourceMembershipStatus
    **/
    public void setSourceMembershipStatus(SourceMembershipStatusEnum sourceMembershipStatus) {
        this.sourceMembershipStatus = sourceMembershipStatus;
    }

    /**
    * Quick Set pipelineFolderStatus
    * Useful setter to use in builder style (eg. myGroup.pipelineFolderStatus(PipelineFolderStatusEnum).anotherQuickSetter(..))
    * @param PipelineFolderStatusEnum pipelineFolderStatus
    * @return Group The modified Group object
    **/
    public Group pipelineFolderStatus(PipelineFolderStatusEnum pipelineFolderStatus) {
        this.pipelineFolderStatus = pipelineFolderStatus;
        return this;
    }

    /**
    * pipelineFolderStatus
    * @return PipelineFolderStatusEnum pipelineFolderStatus
    **/
    @ApiModelProperty(value = "pipelineFolderStatus")
    public PipelineFolderStatusEnum getPipelineFolderStatus() {
        return pipelineFolderStatus;
    }

    /**
    * pipelineFolderStatus
    **/
    public void setPipelineFolderStatus(PipelineFolderStatusEnum pipelineFolderStatus) {
        this.pipelineFolderStatus = pipelineFolderStatus;
    }

    /**
    * Quick Set binaryRepositoryStatus
    * Useful setter to use in builder style (eg. myGroup.binaryRepositoryStatus(BinaryRepositoryStatusEnum).anotherQuickSetter(..))
    * @param BinaryRepositoryStatusEnum binaryRepositoryStatus
    * @return Group The modified Group object
    **/
    public Group binaryRepositoryStatus(BinaryRepositoryStatusEnum binaryRepositoryStatus) {
        this.binaryRepositoryStatus = binaryRepositoryStatus;
        return this;
    }

    /**
    * binaryRepositoryStatus
    * @return BinaryRepositoryStatusEnum binaryRepositoryStatus
    **/
    @ApiModelProperty(value = "binaryRepositoryStatus")
    public BinaryRepositoryStatusEnum getBinaryRepositoryStatus() {
        return binaryRepositoryStatus;
    }

    /**
    * binaryRepositoryStatus
    **/
    public void setBinaryRepositoryStatus(BinaryRepositoryStatusEnum binaryRepositoryStatus) {
        this.binaryRepositoryStatus = binaryRepositoryStatus;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return Objects.equals(this.path, group.path) &&
        Objects.equals(this.technicalUser, group.technicalUser) &&
        Objects.equals(this.binaryRepositories, group.binaryRepositories) &&
        Objects.equals(this.members, group.members) &&
        Objects.equals(this.parent, group.parent) &&
        Objects.equals(this.sourceRepositoryStatus, group.sourceRepositoryStatus) &&
        Objects.equals(this.sourceMembershipStatus, group.sourceMembershipStatus) &&
        Objects.equals(this.pipelineFolderStatus, group.pipelineFolderStatus) &&
        Objects.equals(this.binaryRepositoryStatus, group.binaryRepositoryStatus) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, technicalUser, binaryRepositories, members, parent, sourceRepositoryStatus, sourceMembershipStatus, pipelineFolderStatus, binaryRepositoryStatus, super.hashCode());
    }
}

