/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import java.util.Objects;

/**
* Asset
*/
@ApiModel(description = "Asset")
public class Asset extends Resource {

    protected BinaryRepository binaryRepository = null;
    protected SourceRepository sourceRepository = null;
    protected Pipeline pipeline = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myAsset.id(String).anotherQuickSetter(..))
    * @param String id
    * @return Asset The modified Asset object
    **/
    public Asset id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myAsset.name(String).anotherQuickSetter(..))
    * @param String name
    * @return Asset The modified Asset object
    **/
    public Asset name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myAsset.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return Asset The modified Asset object
    **/
    public Asset status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myAsset.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return Asset The modified Asset object
    **/
    public Asset createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myAsset.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return Asset The modified Asset object
    **/
    public Asset updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myAsset.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return Asset The modified Asset object
    **/
    public Asset createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myAsset.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return Asset The modified Asset object
    **/
    public Asset updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myAsset.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return Asset The modified Asset object
    **/
    public Asset metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public Asset putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set binaryRepository
    * Useful setter to use in builder style (eg. myAsset.binaryRepository(BinaryRepository).anotherQuickSetter(..))
    * @param BinaryRepository binaryRepository
    * @return Asset The modified Asset object
    **/
    public Asset binaryRepository(BinaryRepository binaryRepository) {
        this.binaryRepository = binaryRepository;
        return this;
    }

    /**
    * binaryRepository
    * @return BinaryRepository binaryRepository
    **/
    @ApiModelProperty(value = "binaryRepository")
    public BinaryRepository getBinaryRepository() {
        return binaryRepository;
    }

    /**
    * binaryRepository
    **/
    public void setBinaryRepository(BinaryRepository binaryRepository) {
        this.binaryRepository = binaryRepository;
    }

    /**
    * Quick Set sourceRepository
    * Useful setter to use in builder style (eg. myAsset.sourceRepository(SourceRepository).anotherQuickSetter(..))
    * @param SourceRepository sourceRepository
    * @return Asset The modified Asset object
    **/
    public Asset sourceRepository(SourceRepository sourceRepository) {
        this.sourceRepository = sourceRepository;
        return this;
    }

    /**
    * sourceRepository
    * @return SourceRepository sourceRepository
    **/
    @ApiModelProperty(value = "sourceRepository")
    public SourceRepository getSourceRepository() {
        return sourceRepository;
    }

    /**
    * sourceRepository
    **/
    public void setSourceRepository(SourceRepository sourceRepository) {
        this.sourceRepository = sourceRepository;
    }

    /**
    * Quick Set pipeline
    * Useful setter to use in builder style (eg. myAsset.pipeline(Pipeline).anotherQuickSetter(..))
    * @param Pipeline pipeline
    * @return Asset The modified Asset object
    **/
    public Asset pipeline(Pipeline pipeline) {
        this.pipeline = pipeline;
        return this;
    }

    /**
    * pipeline
    * @return Pipeline pipeline
    **/
    @ApiModelProperty(value = "pipeline")
    public Pipeline getPipeline() {
        return pipeline;
    }

    /**
    * pipeline
    **/
    public void setPipeline(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Asset asset = (Asset) o;
        return Objects.equals(this.binaryRepository, asset.binaryRepository) &&
        Objects.equals(this.sourceRepository, asset.sourceRepository) &&
        Objects.equals(this.pipeline, asset.pipeline) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(binaryRepository, sourceRepository, pipeline, super.hashCode());
    }
}

