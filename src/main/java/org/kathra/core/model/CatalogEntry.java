/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* CatalogEntry
*/
@ApiModel(description = "CatalogEntry")
public class CatalogEntry extends Resource {

    protected String description = null;
    protected String icon = null;
    /**
    * packageTemplate
    */
    public enum PackageTemplateEnum {
        SERVICE("REST_SERVICE");

        private String value;

        PackageTemplateEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static PackageTemplateEnum fromValue(String text) {
            for (PackageTemplateEnum b : PackageTemplateEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected PackageTemplateEnum packageTemplate = null;
    protected List<CatalogEntryPackage> packages = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myCatalogEntry.id(String).anotherQuickSetter(..))
    * @param String id
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myCatalogEntry.name(String).anotherQuickSetter(..))
    * @param String name
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myCatalogEntry.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myCatalogEntry.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myCatalogEntry.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myCatalogEntry.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myCatalogEntry.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myCatalogEntry.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public CatalogEntry putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set description
    * Useful setter to use in builder style (eg. myCatalogEntry.description(String).anotherQuickSetter(..))
    * @param String description
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry description(String description) {
        this.description = description;
        return this;
    }

    /**
    * Description
    * @return String description
    **/
    @ApiModelProperty(value = "Description")
    public String getDescription() {
        return description;
    }

    /**
    * Description
    **/
    public void setDescription(String description) {
        this.description = description;
    }

    /**
    * Quick Set icon
    * Useful setter to use in builder style (eg. myCatalogEntry.icon(String).anotherQuickSetter(..))
    * @param String icon
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry icon(String icon) {
        this.icon = icon;
        return this;
    }

    /**
    * Icon's URL
    * @return String icon
    **/
    @ApiModelProperty(value = "Icon's URL")
    public String getIcon() {
        return icon;
    }

    /**
    * Icon's URL
    **/
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
    * Quick Set packageTemplate
    * Useful setter to use in builder style (eg. myCatalogEntry.packageTemplate(PackageTemplateEnum).anotherQuickSetter(..))
    * @param PackageTemplateEnum packageTemplate
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry packageTemplate(PackageTemplateEnum packageTemplate) {
        this.packageTemplate = packageTemplate;
        return this;
    }

    /**
    * packageTemplate
    * @return PackageTemplateEnum packageTemplate
    **/
    @ApiModelProperty(value = "packageTemplate")
    public PackageTemplateEnum getPackageTemplate() {
        return packageTemplate;
    }

    /**
    * packageTemplate
    **/
    public void setPackageTemplate(PackageTemplateEnum packageTemplate) {
        this.packageTemplate = packageTemplate;
    }

    /**
    * Quick Set packages
    * Useful setter to use in builder style (eg. myCatalogEntry.packages(List<CatalogEntryPackage>).anotherQuickSetter(..))
    * @param List<CatalogEntryPackage> packages
    * @return CatalogEntry The modified CatalogEntry object
    **/
    public CatalogEntry packages(List<CatalogEntryPackage> packages) {
        this.packages = packages;
        return this;
    }

    public CatalogEntry addPackagesItem(CatalogEntryPackage packagesItem) {
        if (this.packages == null) {
            this.packages = new ArrayList();
        }
        this.packages.add(packagesItem);
        return this;
    }

    /**
    * packages
    * @return List<CatalogEntryPackage> packages
    **/
    @ApiModelProperty(value = "packages")
    public List<CatalogEntryPackage> getPackages() {
        return packages;
    }

    /**
    * packages
    **/
    public void setPackages(List<CatalogEntryPackage> packages) {
        this.packages = packages;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CatalogEntry catalogEntry = (CatalogEntry) o;
        return Objects.equals(this.description, catalogEntry.description) &&
        Objects.equals(this.icon, catalogEntry.icon) &&
        Objects.equals(this.packageTemplate, catalogEntry.packageTemplate) &&
        Objects.equals(this.packages, catalogEntry.packages) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, icon, packageTemplate, packages, super.hashCode());
    }
}

