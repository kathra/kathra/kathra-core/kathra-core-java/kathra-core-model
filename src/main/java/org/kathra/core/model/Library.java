/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* Library
*/
@ApiModel(description = "Library")
public class Library extends Asset {

    protected Component component = null;
    /**
    * type
    */
    public enum TypeEnum {
        MODEL("MODEL"),
        
        INTERFACE("INTERFACE"),
        
        CLIENT("CLIENT");

        private String value;

        TypeEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static TypeEnum fromValue(String text) {
            for (TypeEnum b : TypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected TypeEnum type = null;
    /**
    * language
    */
    public enum LanguageEnum {
        JAVA("JAVA"),
        
        PYTHON("PYTHON");

        private String value;

        LanguageEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static LanguageEnum fromValue(String text) {
            for (LanguageEnum b : LanguageEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected LanguageEnum language = null;
    protected List<LibraryApiVersion> versions = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myLibrary.id(String).anotherQuickSetter(..))
    * @param String id
    * @return Library The modified Library object
    **/
    public Library id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myLibrary.name(String).anotherQuickSetter(..))
    * @param String name
    * @return Library The modified Library object
    **/
    public Library name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myLibrary.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return Library The modified Library object
    **/
    public Library status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myLibrary.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return Library The modified Library object
    **/
    public Library createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myLibrary.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return Library The modified Library object
    **/
    public Library updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myLibrary.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return Library The modified Library object
    **/
    public Library createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myLibrary.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return Library The modified Library object
    **/
    public Library updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myLibrary.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return Library The modified Library object
    **/
    public Library metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public Library putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }
    /**
    * Quick Set binaryRepository
    * Useful setter to use in builder style (eg. myLibrary.binaryRepository(BinaryRepository).anotherQuickSetter(..))
    * @param BinaryRepository binaryRepository
    * @return Library The modified Library object
    **/
    public Library binaryRepository(BinaryRepository binaryRepository) {
        this.binaryRepository = binaryRepository;
        return this;
    }

    /**
    * Quick Set sourceRepository
    * Useful setter to use in builder style (eg. myLibrary.sourceRepository(SourceRepository).anotherQuickSetter(..))
    * @param SourceRepository sourceRepository
    * @return Library The modified Library object
    **/
    public Library sourceRepository(SourceRepository sourceRepository) {
        this.sourceRepository = sourceRepository;
        return this;
    }

    /**
    * Quick Set pipeline
    * Useful setter to use in builder style (eg. myLibrary.pipeline(Pipeline).anotherQuickSetter(..))
    * @param Pipeline pipeline
    * @return Library The modified Library object
    **/
    public Library pipeline(Pipeline pipeline) {
        this.pipeline = pipeline;
        return this;
    }


    /**
    * Quick Set component
    * Useful setter to use in builder style (eg. myLibrary.component(Component).anotherQuickSetter(..))
    * @param Component component
    * @return Library The modified Library object
    **/
    public Library component(Component component) {
        this.component = component;
        return this;
    }

    /**
    * component
    * @return Component component
    **/
    @ApiModelProperty(value = "component")
    public Component getComponent() {
        return component;
    }

    /**
    * component
    **/
    public void setComponent(Component component) {
        this.component = component;
    }

    /**
    * Quick Set type
    * Useful setter to use in builder style (eg. myLibrary.type(TypeEnum).anotherQuickSetter(..))
    * @param TypeEnum type
    * @return Library The modified Library object
    **/
    public Library type(TypeEnum type) {
        this.type = type;
        return this;
    }

    /**
    * type
    * @return TypeEnum type
    **/
    @ApiModelProperty(value = "type")
    public TypeEnum getType() {
        return type;
    }

    /**
    * type
    **/
    public void setType(TypeEnum type) {
        this.type = type;
    }

    /**
    * Quick Set language
    * Useful setter to use in builder style (eg. myLibrary.language(LanguageEnum).anotherQuickSetter(..))
    * @param LanguageEnum language
    * @return Library The modified Library object
    **/
    public Library language(LanguageEnum language) {
        this.language = language;
        return this;
    }

    /**
    * language
    * @return LanguageEnum language
    **/
    @ApiModelProperty(value = "language")
    public LanguageEnum getLanguage() {
        return language;
    }

    /**
    * language
    **/
    public void setLanguage(LanguageEnum language) {
        this.language = language;
    }

    /**
    * Quick Set versions
    * Useful setter to use in builder style (eg. myLibrary.versions(List<LibraryApiVersion>).anotherQuickSetter(..))
    * @param List<LibraryApiVersion> versions
    * @return Library The modified Library object
    **/
    public Library versions(List<LibraryApiVersion> versions) {
        this.versions = versions;
        return this;
    }

    public Library addVersionsItem(LibraryApiVersion versionsItem) {
        if (this.versions == null) {
            this.versions = new ArrayList();
        }
        this.versions.add(versionsItem);
        return this;
    }

    /**
    * versions
    * @return List<LibraryApiVersion> versions
    **/
    @ApiModelProperty(value = "versions")
    public List<LibraryApiVersion> getVersions() {
        return versions;
    }

    /**
    * versions
    **/
    public void setVersions(List<LibraryApiVersion> versions) {
        this.versions = versions;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Library library = (Library) o;
        return Objects.equals(this.component, library.component) &&
        Objects.equals(this.type, library.type) &&
        Objects.equals(this.language, library.language) &&
        Objects.equals(this.versions, library.versions) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(component, type, language, versions, super.hashCode());
    }
}

