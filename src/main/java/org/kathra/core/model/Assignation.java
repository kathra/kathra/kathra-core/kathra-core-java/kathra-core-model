/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import java.util.Objects;

/**
* Assignation
*/
@ApiModel(description = "Assignation")
public class Assignation extends Resource {

    protected String role = null;
    protected String fte = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myAssignation.id(String).anotherQuickSetter(..))
    * @param String id
    * @return Assignation The modified Assignation object
    **/
    public Assignation id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myAssignation.name(String).anotherQuickSetter(..))
    * @param String name
    * @return Assignation The modified Assignation object
    **/
    public Assignation name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myAssignation.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return Assignation The modified Assignation object
    **/
    public Assignation status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myAssignation.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return Assignation The modified Assignation object
    **/
    public Assignation createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myAssignation.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return Assignation The modified Assignation object
    **/
    public Assignation updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myAssignation.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return Assignation The modified Assignation object
    **/
    public Assignation createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myAssignation.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return Assignation The modified Assignation object
    **/
    public Assignation updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myAssignation.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return Assignation The modified Assignation object
    **/
    public Assignation metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public Assignation putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set role
    * Useful setter to use in builder style (eg. myAssignation.role(String).anotherQuickSetter(..))
    * @param String role
    * @return Assignation The modified Assignation object
    **/
    public Assignation role(String role) {
        this.role = role;
        return this;
    }

    /**
    * role
    * @return String role
    **/
    @ApiModelProperty(value = "role")
    public String getRole() {
        return role;
    }

    /**
    * role
    **/
    public void setRole(String role) {
        this.role = role;
    }

    /**
    * Quick Set fte
    * Useful setter to use in builder style (eg. myAssignation.fte(String).anotherQuickSetter(..))
    * @param String fte
    * @return Assignation The modified Assignation object
    **/
    public Assignation fte(String fte) {
        this.fte = fte;
        return this;
    }

    /**
    * fte
    * @return String fte
    **/
    @ApiModelProperty(value = "fte")
    public String getFte() {
        return fte;
    }

    /**
    * fte
    **/
    public void setFte(String fte) {
        this.fte = fte;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Assignation assignation = (Assignation) o;
        return Objects.equals(this.role, assignation.role) &&
        Objects.equals(this.fte, assignation.fte) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(role, fte, super.hashCode());
    }
}

