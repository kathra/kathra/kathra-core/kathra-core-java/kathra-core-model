/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
* Build
*/
@ApiModel(description = "Build")
public class Build {

    protected String buildNumber = null;
    protected String path = null;
    protected String commitLabel = null;
    protected String commitId = null;
    protected Long creationDate = null;
    protected Integer duration = null;
    /**
    * Build status
    */
    public enum StatusEnum {
        SCHEDULED("scheduled"),
        
        PROCESSING("processing"),
        
        SUCCESS("success"),
        
        FAILED("failed");

        private String value;

        StatusEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static StatusEnum fromValue(String text) {
            for (StatusEnum b : StatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected StatusEnum status = null;
    protected List<BuildArgument> buildArguments = null;
    protected String logs = null;


    /**
    * Quick Set buildNumber
    * Useful setter to use in builder style (eg. myBuild.buildNumber(String).anotherQuickSetter(..))
    * @param String buildNumber
    * @return Build The modified Build object
    **/
    public Build buildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
        return this;
    }

    /**
    * Build identifier
    * @return String buildNumber
    **/
    @ApiModelProperty(value = "Build identifier")
    public String getBuildNumber() {
        return buildNumber;
    }

    /**
    * Build identifier
    **/
    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    /**
    * Quick Set path
    * Useful setter to use in builder style (eg. myBuild.path(String).anotherQuickSetter(..))
    * @param String path
    * @return Build The modified Build object
    **/
    public Build path(String path) {
        this.path = path;
        return this;
    }

    /**
    * Pipeline path
    * @return String path
    **/
    @ApiModelProperty(value = "Pipeline path")
    public String getPath() {
        return path;
    }

    /**
    * Pipeline path
    **/
    public void setPath(String path) {
        this.path = path;
    }

    /**
    * Quick Set commitLabel
    * Useful setter to use in builder style (eg. myBuild.commitLabel(String).anotherQuickSetter(..))
    * @param String commitLabel
    * @return Build The modified Build object
    **/
    public Build commitLabel(String commitLabel) {
        this.commitLabel = commitLabel;
        return this;
    }

    /**
    * Branch or Tag
    * @return String commitLabel
    **/
    @ApiModelProperty(value = "Branch or Tag")
    public String getCommitLabel() {
        return commitLabel;
    }

    /**
    * Branch or Tag
    **/
    public void setCommitLabel(String commitLabel) {
        this.commitLabel = commitLabel;
    }

    /**
    * Quick Set commitId
    * Useful setter to use in builder style (eg. myBuild.commitId(String).anotherQuickSetter(..))
    * @param String commitId
    * @return Build The modified Build object
    **/
    public Build commitId(String commitId) {
        this.commitId = commitId;
        return this;
    }

    /**
    * Commit id
    * @return String commitId
    **/
    @ApiModelProperty(value = "Commit id")
    public String getCommitId() {
        return commitId;
    }

    /**
    * Commit id
    **/
    public void setCommitId(String commitId) {
        this.commitId = commitId;
    }

    /**
    * Quick Set creationDate
    * Useful setter to use in builder style (eg. myBuild.creationDate(Long).anotherQuickSetter(..))
    * @param Long creationDate
    * @return Build The modified Build object
    **/
    public Build creationDate(Long creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    /**
    * Build date (UTC timestamp)
    * @return Long creationDate
    **/
    @ApiModelProperty(value = "Build date (UTC timestamp)")
    public Long getCreationDate() {
        return creationDate;
    }

    /**
    * Build date (UTC timestamp)
    **/
    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    /**
    * Quick Set duration
    * Useful setter to use in builder style (eg. myBuild.duration(Integer).anotherQuickSetter(..))
    * @param Integer duration
    * @return Build The modified Build object
    **/
    public Build duration(Integer duration) {
        this.duration = duration;
        return this;
    }

    /**
    * Duration in seconds
    * @return Integer duration
    **/
    @ApiModelProperty(value = "Duration in seconds")
    public Integer getDuration() {
        return duration;
    }

    /**
    * Duration in seconds
    **/
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myBuild.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return Build The modified Build object
    **/
    public Build status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Build status
    * @return StatusEnum status
    **/
    @ApiModelProperty(value = "Build status")
    public StatusEnum getStatus() {
        return status;
    }

    /**
    * Build status
    **/
    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    /**
    * Quick Set buildArguments
    * Useful setter to use in builder style (eg. myBuild.buildArguments(List<BuildArgument>).anotherQuickSetter(..))
    * @param List<BuildArgument> buildArguments
    * @return Build The modified Build object
    **/
    public Build buildArguments(List<BuildArgument> buildArguments) {
        this.buildArguments = buildArguments;
        return this;
    }

    public Build addBuildArgumentsItem(BuildArgument buildArgumentsItem) {
        if (this.buildArguments == null) {
            this.buildArguments = new ArrayList();
        }
        this.buildArguments.add(buildArgumentsItem);
        return this;
    }

    /**
    * Additional Build arguments
    * @return List<BuildArgument> buildArguments
    **/
    @ApiModelProperty(value = "Additional Build arguments")
    public List<BuildArgument> getBuildArguments() {
        return buildArguments;
    }

    /**
    * Additional Build arguments
    **/
    public void setBuildArguments(List<BuildArgument> buildArguments) {
        this.buildArguments = buildArguments;
    }

    /**
    * Quick Set logs
    * Useful setter to use in builder style (eg. myBuild.logs(String).anotherQuickSetter(..))
    * @param String logs
    * @return Build The modified Build object
    **/
    public Build logs(String logs) {
        this.logs = logs;
        return this;
    }

    /**
    * Build logs
    * @return String logs
    **/
    @ApiModelProperty(value = "Build logs")
    public String getLogs() {
        return logs;
    }

    /**
    * Build logs
    **/
    public void setLogs(String logs) {
        this.logs = logs;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Build build = (Build) o;
        return Objects.equals(this.buildNumber, build.buildNumber) &&
        Objects.equals(this.path, build.path) &&
        Objects.equals(this.commitLabel, build.commitLabel) &&
        Objects.equals(this.commitId, build.commitId) &&
        Objects.equals(this.creationDate, build.creationDate) &&
        Objects.equals(this.duration, build.duration) &&
        Objects.equals(this.status, build.status) &&
        Objects.equals(this.buildArguments, build.buildArguments) &&
        Objects.equals(this.logs, build.logs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(buildNumber, path, commitLabel, commitId, creationDate, duration, status, buildArguments, logs);
    }
}

