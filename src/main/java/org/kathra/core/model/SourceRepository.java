/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* SourceRepository
*/
@ApiModel(description = "SourceRepository")
public class SourceRepository extends Resource {

    protected String provider = null;
    protected String providerId = null;
    protected String path = null;
    protected String sshUrl = null;
    protected String httpUrl = null;
    protected String webUrl = null;
    protected List<String> branchs = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. mySourceRepository.id(String).anotherQuickSetter(..))
    * @param String id
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. mySourceRepository.name(String).anotherQuickSetter(..))
    * @param String name
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. mySourceRepository.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. mySourceRepository.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. mySourceRepository.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. mySourceRepository.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. mySourceRepository.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. mySourceRepository.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public SourceRepository putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set provider
    * Useful setter to use in builder style (eg. mySourceRepository.provider(String).anotherQuickSetter(..))
    * @param String provider
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository provider(String provider) {
        this.provider = provider;
        return this;
    }

    /**
    * provider
    * @return String provider
    **/
    @ApiModelProperty(value = "provider")
    public String getProvider() {
        return provider;
    }

    /**
    * provider
    **/
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
    * Quick Set providerId
    * Useful setter to use in builder style (eg. mySourceRepository.providerId(String).anotherQuickSetter(..))
    * @param String providerId
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository providerId(String providerId) {
        this.providerId = providerId;
        return this;
    }

    /**
    * providerId
    * @return String providerId
    **/
    @ApiModelProperty(value = "providerId")
    public String getProviderId() {
        return providerId;
    }

    /**
    * providerId
    **/
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    /**
    * Quick Set path
    * Useful setter to use in builder style (eg. mySourceRepository.path(String).anotherQuickSetter(..))
    * @param String path
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository path(String path) {
        this.path = path;
        return this;
    }

    /**
    * path
    * @return String path
    **/
    @ApiModelProperty(value = "path")
    public String getPath() {
        return path;
    }

    /**
    * path
    **/
    public void setPath(String path) {
        this.path = path;
    }

    /**
    * Quick Set sshUrl
    * Useful setter to use in builder style (eg. mySourceRepository.sshUrl(String).anotherQuickSetter(..))
    * @param String sshUrl
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository sshUrl(String sshUrl) {
        this.sshUrl = sshUrl;
        return this;
    }

    /**
    * sshUrl
    * @return String sshUrl
    **/
    @ApiModelProperty(value = "sshUrl")
    public String getSshUrl() {
        return sshUrl;
    }

    /**
    * sshUrl
    **/
    public void setSshUrl(String sshUrl) {
        this.sshUrl = sshUrl;
    }

    /**
    * Quick Set httpUrl
    * Useful setter to use in builder style (eg. mySourceRepository.httpUrl(String).anotherQuickSetter(..))
    * @param String httpUrl
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository httpUrl(String httpUrl) {
        this.httpUrl = httpUrl;
        return this;
    }

    /**
    * httpUrl
    * @return String httpUrl
    **/
    @ApiModelProperty(value = "httpUrl")
    public String getHttpUrl() {
        return httpUrl;
    }

    /**
    * httpUrl
    **/
    public void setHttpUrl(String httpUrl) {
        this.httpUrl = httpUrl;
    }

    /**
    * Quick Set webUrl
    * Useful setter to use in builder style (eg. mySourceRepository.webUrl(String).anotherQuickSetter(..))
    * @param String webUrl
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository webUrl(String webUrl) {
        this.webUrl = webUrl;
        return this;
    }

    /**
    * webUrl
    * @return String webUrl
    **/
    @ApiModelProperty(value = "webUrl")
    public String getWebUrl() {
        return webUrl;
    }

    /**
    * webUrl
    **/
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    /**
    * Quick Set branchs
    * Useful setter to use in builder style (eg. mySourceRepository.branchs(List<String>).anotherQuickSetter(..))
    * @param List<String> branchs
    * @return SourceRepository The modified SourceRepository object
    **/
    public SourceRepository branchs(List<String> branchs) {
        this.branchs = branchs;
        return this;
    }

    public SourceRepository addBranchsItem(String branchsItem) {
        if (this.branchs == null) {
            this.branchs = new ArrayList();
        }
        this.branchs.add(branchsItem);
        return this;
    }

    /**
    * branchs
    * @return List<String> branchs
    **/
    @ApiModelProperty(value = "branchs")
    public List<String> getBranchs() {
        return branchs;
    }

    /**
    * branchs
    **/
    public void setBranchs(List<String> branchs) {
        this.branchs = branchs;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SourceRepository sourceRepository = (SourceRepository) o;
        return Objects.equals(this.provider, sourceRepository.provider) &&
        Objects.equals(this.providerId, sourceRepository.providerId) &&
        Objects.equals(this.path, sourceRepository.path) &&
        Objects.equals(this.sshUrl, sourceRepository.sshUrl) &&
        Objects.equals(this.httpUrl, sourceRepository.httpUrl) &&
        Objects.equals(this.webUrl, sourceRepository.webUrl) &&
        Objects.equals(this.branchs, sourceRepository.branchs) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(provider, providerId, path, sshUrl, httpUrl, webUrl, branchs, super.hashCode());
    }
}

