/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import java.util.Objects;

/**
* LibraryApiVersion
*/
@ApiModel(description = "LibraryApiVersion")
public class LibraryApiVersion extends Resource {

    protected Library library = null;
    protected ApiVersion apiVersion = null;
    /**
    * apiRepositoryStatus
    */
    public enum ApiRepositoryStatusEnum {
        PENDING("PENDING"),
        
        READY("READY"),
        
        UPDATING("UPDATING"),
        
        UNSTABLE("UNSTABLE"),
        
        ERROR("ERROR"),
        
        DELETED("DELETED");

        private String value;

        ApiRepositoryStatusEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static ApiRepositoryStatusEnum fromValue(String text) {
            for (ApiRepositoryStatusEnum b : ApiRepositoryStatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected ApiRepositoryStatusEnum apiRepositoryStatus = null;
    /**
    * pipelineStatus
    */
    public enum PipelineStatusEnum {
        PENDING("PENDING"),
        
        READY("READY"),
        
        UPDATING("UPDATING"),
        
        UNSTABLE("UNSTABLE"),
        
        ERROR("ERROR"),
        
        DELETED("DELETED");

        private String value;

        PipelineStatusEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static PipelineStatusEnum fromValue(String text) {
            for (PipelineStatusEnum b : PipelineStatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected PipelineStatusEnum pipelineStatus = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myLibraryApiVersion.id(String).anotherQuickSetter(..))
    * @param String id
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myLibraryApiVersion.name(String).anotherQuickSetter(..))
    * @param String name
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myLibraryApiVersion.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myLibraryApiVersion.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myLibraryApiVersion.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myLibraryApiVersion.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myLibraryApiVersion.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myLibraryApiVersion.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public LibraryApiVersion putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }

    /**
    * Quick Set library
    * Useful setter to use in builder style (eg. myLibraryApiVersion.library(Library).anotherQuickSetter(..))
    * @param Library library
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion library(Library library) {
        this.library = library;
        return this;
    }

    /**
    * library
    * @return Library library
    **/
    @ApiModelProperty(value = "library")
    public Library getLibrary() {
        return library;
    }

    /**
    * library
    **/
    public void setLibrary(Library library) {
        this.library = library;
    }

    /**
    * Quick Set apiVersion
    * Useful setter to use in builder style (eg. myLibraryApiVersion.apiVersion(ApiVersion).anotherQuickSetter(..))
    * @param ApiVersion apiVersion
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion apiVersion(ApiVersion apiVersion) {
        this.apiVersion = apiVersion;
        return this;
    }

    /**
    * apiVersion
    * @return ApiVersion apiVersion
    **/
    @ApiModelProperty(value = "apiVersion")
    public ApiVersion getApiVersion() {
        return apiVersion;
    }

    /**
    * apiVersion
    **/
    public void setApiVersion(ApiVersion apiVersion) {
        this.apiVersion = apiVersion;
    }

    /**
    * Quick Set apiRepositoryStatus
    * Useful setter to use in builder style (eg. myLibraryApiVersion.apiRepositoryStatus(ApiRepositoryStatusEnum).anotherQuickSetter(..))
    * @param ApiRepositoryStatusEnum apiRepositoryStatus
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion apiRepositoryStatus(ApiRepositoryStatusEnum apiRepositoryStatus) {
        this.apiRepositoryStatus = apiRepositoryStatus;
        return this;
    }

    /**
    * apiRepositoryStatus
    * @return ApiRepositoryStatusEnum apiRepositoryStatus
    **/
    @ApiModelProperty(value = "apiRepositoryStatus")
    public ApiRepositoryStatusEnum getApiRepositoryStatus() {
        return apiRepositoryStatus;
    }

    /**
    * apiRepositoryStatus
    **/
    public void setApiRepositoryStatus(ApiRepositoryStatusEnum apiRepositoryStatus) {
        this.apiRepositoryStatus = apiRepositoryStatus;
    }

    /**
    * Quick Set pipelineStatus
    * Useful setter to use in builder style (eg. myLibraryApiVersion.pipelineStatus(PipelineStatusEnum).anotherQuickSetter(..))
    * @param PipelineStatusEnum pipelineStatus
    * @return LibraryApiVersion The modified LibraryApiVersion object
    **/
    public LibraryApiVersion pipelineStatus(PipelineStatusEnum pipelineStatus) {
        this.pipelineStatus = pipelineStatus;
        return this;
    }

    /**
    * pipelineStatus
    * @return PipelineStatusEnum pipelineStatus
    **/
    @ApiModelProperty(value = "pipelineStatus")
    public PipelineStatusEnum getPipelineStatus() {
        return pipelineStatus;
    }

    /**
    * pipelineStatus
    **/
    public void setPipelineStatus(PipelineStatusEnum pipelineStatus) {
        this.pipelineStatus = pipelineStatus;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LibraryApiVersion libraryApiVersion = (LibraryApiVersion) o;
        return Objects.equals(this.library, libraryApiVersion.library) &&
        Objects.equals(this.apiVersion, libraryApiVersion.apiVersion) &&
        Objects.equals(this.apiRepositoryStatus, libraryApiVersion.apiRepositoryStatus) &&
        Objects.equals(this.pipelineStatus, libraryApiVersion.pipelineStatus) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(library, apiVersion, apiRepositoryStatus, pipelineStatus, super.hashCode());
    }
}

