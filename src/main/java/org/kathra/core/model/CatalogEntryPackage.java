/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* CatalogEntryPackage
*/
@ApiModel(description = "CatalogEntryPackage")
public class CatalogEntryPackage extends Asset {

    protected CatalogEntry catalogEntry = null;
    /**
    * packageType
    */
    public enum PackageTypeEnum {
        HELM("HELM");

        private String value;

        PackageTypeEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static PackageTypeEnum fromValue(String text) {
            for (PackageTypeEnum b : PackageTypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected PackageTypeEnum packageType = null;
    protected String provider = null;
    protected String providerId = null;
    protected String url = null;
    protected List<CatalogEntryPackageVersion> versions = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.id(String).anotherQuickSetter(..))
    * @param String id
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage id(String id) {
        this.id = id;
        return this;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.name(String).anotherQuickSetter(..))
    * @param String name
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * Quick Set createdBy
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.createdBy(String).anotherQuickSetter(..))
    * @param String createdBy
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
    * Quick Set updatedBy
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.updatedBy(String).anotherQuickSetter(..))
    * @param String updatedBy
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    /**
    * Quick Set createdAt
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.createdAt(Integer).anotherQuickSetter(..))
    * @param Integer createdAt
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage createdAt(Integer createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
    * Quick Set updatedAt
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.updatedAt(Integer).anotherQuickSetter(..))
    * @param Integer updatedAt
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage updatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public CatalogEntryPackage putMetadataItem(String key, Object metadataItem) {
        super.putMetadataItem(key, metadataItem);
        return this;
    }
    /**
    * Quick Set binaryRepository
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.binaryRepository(BinaryRepository).anotherQuickSetter(..))
    * @param BinaryRepository binaryRepository
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage binaryRepository(BinaryRepository binaryRepository) {
        this.binaryRepository = binaryRepository;
        return this;
    }

    /**
    * Quick Set sourceRepository
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.sourceRepository(SourceRepository).anotherQuickSetter(..))
    * @param SourceRepository sourceRepository
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage sourceRepository(SourceRepository sourceRepository) {
        this.sourceRepository = sourceRepository;
        return this;
    }

    /**
    * Quick Set pipeline
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.pipeline(Pipeline).anotherQuickSetter(..))
    * @param Pipeline pipeline
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage pipeline(Pipeline pipeline) {
        this.pipeline = pipeline;
        return this;
    }


    /**
    * Quick Set catalogEntry
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.catalogEntry(CatalogEntry).anotherQuickSetter(..))
    * @param CatalogEntry catalogEntry
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage catalogEntry(CatalogEntry catalogEntry) {
        this.catalogEntry = catalogEntry;
        return this;
    }

    /**
    * catalogEntry
    * @return CatalogEntry catalogEntry
    **/
    @ApiModelProperty(value = "catalogEntry")
    public CatalogEntry getCatalogEntry() {
        return catalogEntry;
    }

    /**
    * catalogEntry
    **/
    public void setCatalogEntry(CatalogEntry catalogEntry) {
        this.catalogEntry = catalogEntry;
    }

    /**
    * Quick Set packageType
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.packageType(PackageTypeEnum).anotherQuickSetter(..))
    * @param PackageTypeEnum packageType
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage packageType(PackageTypeEnum packageType) {
        this.packageType = packageType;
        return this;
    }

    /**
    * packageType
    * @return PackageTypeEnum packageType
    **/
    @ApiModelProperty(value = "packageType")
    public PackageTypeEnum getPackageType() {
        return packageType;
    }

    /**
    * packageType
    **/
    public void setPackageType(PackageTypeEnum packageType) {
        this.packageType = packageType;
    }

    /**
    * Quick Set provider
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.provider(String).anotherQuickSetter(..))
    * @param String provider
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage provider(String provider) {
        this.provider = provider;
        return this;
    }

    /**
    * Provider
    * @return String provider
    **/
    @ApiModelProperty(value = "Provider")
    public String getProvider() {
        return provider;
    }

    /**
    * Provider
    **/
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
    * Quick Set providerId
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.providerId(String).anotherQuickSetter(..))
    * @param String providerId
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage providerId(String providerId) {
        this.providerId = providerId;
        return this;
    }

    /**
    * ProviderId
    * @return String providerId
    **/
    @ApiModelProperty(value = "ProviderId")
    public String getProviderId() {
        return providerId;
    }

    /**
    * ProviderId
    **/
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    /**
    * Quick Set url
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.url(String).anotherQuickSetter(..))
    * @param String url
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage url(String url) {
        this.url = url;
        return this;
    }

    /**
    * String
    * @return String url
    **/
    @ApiModelProperty(value = "String")
    public String getUrl() {
        return url;
    }

    /**
    * String
    **/
    public void setUrl(String url) {
        this.url = url;
    }

    /**
    * Quick Set versions
    * Useful setter to use in builder style (eg. myCatalogEntryPackage.versions(List<CatalogEntryPackageVersion>).anotherQuickSetter(..))
    * @param List<CatalogEntryPackageVersion> versions
    * @return CatalogEntryPackage The modified CatalogEntryPackage object
    **/
    public CatalogEntryPackage versions(List<CatalogEntryPackageVersion> versions) {
        this.versions = versions;
        return this;
    }

    public CatalogEntryPackage addVersionsItem(CatalogEntryPackageVersion versionsItem) {
        if (this.versions == null) {
            this.versions = new ArrayList();
        }
        this.versions.add(versionsItem);
        return this;
    }

    /**
    * versions
    * @return List<CatalogEntryPackageVersion> versions
    **/
    @ApiModelProperty(value = "versions")
    public List<CatalogEntryPackageVersion> getVersions() {
        return versions;
    }

    /**
    * versions
    **/
    public void setVersions(List<CatalogEntryPackageVersion> versions) {
        this.versions = versions;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CatalogEntryPackage catalogEntryPackage = (CatalogEntryPackage) o;
        return Objects.equals(this.catalogEntry, catalogEntryPackage.catalogEntry) &&
        Objects.equals(this.packageType, catalogEntryPackage.packageType) &&
        Objects.equals(this.provider, catalogEntryPackage.provider) &&
        Objects.equals(this.providerId, catalogEntryPackage.providerId) &&
        Objects.equals(this.url, catalogEntryPackage.url) &&
        Objects.equals(this.versions, catalogEntryPackage.versions) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(catalogEntry, packageType, provider, providerId, url, versions, super.hashCode());
    }
}

